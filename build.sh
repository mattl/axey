#!/bin/bash
mkdir -p output
rm output/*
cp *.png output/
cp *.jpg output/
cp *.pdf output/
cp *.gif output/
cp *.txt output/

rm *~

for file in *.md
do
    PREFIX=$(basename "$file" | cut -d. -f1)
    if [ $PREFIX == "index" ]
    then sitemap=$sitemap" "
    else sitemap=$sitemap" <li><a href='$PREFIX'>$PREFIX</a></li>"
    fi
done

for file in *.md
do
    PREFIX=$(basename "$file" | cut -d. -f1)
    pandoc $file -T $PREFIX -V "pagetitle:$PREFIX" -V "sitemap:$sitemap" -V "title:$PREFIX" -w html5 -o "output/$PREFIX" --template ./templates/template 
    #pandoc $file -w html -o "$PREFIX" --template ./templates/template
done

