---
Title: Locations
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Locations from the movie

This is not a comprehensive list of locations, as those are already
better served by [Film in America's
page](http://www.filminamerica.com/Movies/SoIMarriedAnAxeMurderer/)
but I will be updating it with modern photos of many of the locations
soon. It'll be fun to see how some of these locations have changed.

Vesuvio Cafe and Edinburgh Castle are still alive and kicking and you
should check them out if you're ever in San Francisco.

