---
Title: Soundtrack
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## The Soundtrack

<p class="tc"><img src="/assets/soundtrack.jpg" width="300" /></p>

The soundtrack to the movie is a great one. It features many
contemporary songs of the era and I like to think Mike Myers himself
was involved in picking some of the tracks. Especially when you
consider the amount of British music and that the title track for the
movie is "There She Goes" a song originally performed by a Liverpool
band. Both the original and a cover by The Boo Radleys are featured in
the movie and on the soundtrack.

1) Boo Radleys - "There She Goes" 2:18
1) Toad the Wet Sprocket - "Brother" 4:04
1) Soul Asylum - "The Break" 2:46
1) Chris Whitley - "Starve To Death" 3:14
1) Big Audio Dynamite II - "Rush" 3:55
1) Mike Myers - "This Poem Sucks" 2:04
1) Ned's Atomic Dustbin - "Saturday Night" 3:08
1) The Darling Buds - "Long Day In The Universe" 4:08
1) The Spin Doctors - "Two Princes" 4:15
1) Suede - "My Insatiable One" 2:57
1) Sun-60 - "Maybe Baby" 3:43
1) The La's - "There She Goes" 2:42

You can [listen to this in a variety of places](https://www.google.com/search?q=so+i+married+an+axe+murderer+soundtrack+stream) or [buy a CD, vinyl or cassette copy of the album](https://www.discogs.com/sell/list?master_id=34310&ev=mb)

### Movie score

The beautiful Bruce Broughton score was also available for a short
while. You can find more information about that at [the Instrada
records website](http://store.intrada.com/s.nl/it.A/id.8316/.f) or
[have a listen to many of the tracks over on YouTube](https://www.youtube.com/watch?v=H1Hm14Cj3Ck&list=PLcKIjMdp0gyv_a5lTphUNLnHVsoZz1C3-).

<img src="/assets/score1.jpg" style="float: right" />

1) Main Title 	2:55
1) Boy Meets Girl 	1:09
1) Weekly World News 	0:38
1) Butcher Shop Montage 	1:58
1) Russian Stroll 	2:23
1) Goin' For It 	2:19
1) Forever Wet 1 	1:00
1) Forever Wet 2 	0:59
1) Creeping Doubt (Revised) 	1:05
1) Ralph 	1:05
1) Name Your Poison 	1:12
1) Ear Needles (Revised) 	0:45
1) Globe Bridge 	1:08
1) Reflections 	0:42
1) You Blew It 	0:27
1) The Bath (Revised) 	0:54
1) She's Guilty (Revised) 	1:04
1) Wedding Bands 	0:32
1) Inn Source #2 	2:14
1) Stalking 	0:42
1) She's Mrs. Axe (Revised) 	0:57
1) The Finale 	5:27
1) The Finale Part 2 	3:13
1) End Theme 	1:35
1) Go See The Folks 	0:17
1) Dinner Drive (Original) 	0:27
1) Goin' For It (Revised) 	2:19
1) Creeping Doubt (Original) 	1:05
1) Ear Needles (Original) 	0:14
1) Globe Bridge (Alternate) 	0:29
1) Second Thoughts 	0:13
1) Second Thoughts (Revised) 	0:12
1) The Bath 	0:52
1) She's Guilty 	1:05
1) She's Mrs. Axe 	0:57
1) The Finale Part 2 (Alternate) 	2:26
1) Inn Source #1 (Haydn Quartet) 	1:46
1) Inn Source #2 (Alternate) 	1:32
1) Only You (And You Alone)	1:31