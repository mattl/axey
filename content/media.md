---
Title: Media
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Media

I've broken this up into a variety of pages.

* [Photos from the movie](photos)
* [Home media](home-media) &mdash; a look at various home media formats including LaserDisc and Video 8, as well as posters.
* [GIFs](gifs) &mdash; I am adding various GIFs you can share with your friends and family here.
* [Interviews](interviews)
