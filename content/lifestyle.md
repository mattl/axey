---
Title: Lifestyle
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## The Lifestyle

Do you find yourself thinking about the movie a lot? Do you find
yourself wanting to visit locations? Do you ever vacation in San
Francisco to be close to the action?

This, then, is the lifestyle.

Welcome.