---
Title: Trivia
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Axey triv

This isn't a page of trivia, it's a page about a trivia night we're hosting.

Details to follow but let's just say it'll be amazing.

Boston-area, August 2018.