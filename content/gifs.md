---
Title: GIFs (pronounced JIFs)
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## GIFs

GIF (pronounced like jiff, not with a hard g) is an image format created by CompuServe. It supports simple animation. Many people have said they like to include one or more GIFs in their electronic mails or post them on IRC.

I am working on these now.

<img src="/assets/axey.gif" />

<img src="/assets/under-construction.gif" />