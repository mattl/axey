---
Title: Home Media
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## 8mm version

<a href="/assets/8mm.jpg"><img src="/assets/thumb-8mm.jpg"></a>

For a movie in 1993, it does seem a little unusual to have a release in 8mm video tape, but here it is.


## Australian VHS

<a href="/assets/aus-vhs-front.jpg"><img src="/assets/thumb-aus-vhs-front.jpg"></a>
<a href="/assets/aus-vhs-back.jpg"><img src="/assets/thumb-aus-vhs-back.jpg"></a>

The front and back of the Australian VHS tape. This poster design was also used in Germany.

<a href="/assets/german.jpg"><img src="/assets/thumb-german.jpg"></a>

## Soundtrack and score albums

You can find more about this on the [soundtrack](soundtrack) page.

<a href="/assets/soundtrack.jpg"><img src="/assets/thumb-soundtrack.jpg"></a>

and

<a href="/assets/score1.jpg"><img src="/assets/thumb-score1.jpg"></a>

## Laserdisc

All the best 1990s movies came out on LaserDisc and this was no exception

<a href="/assets/ld-front.jpg"><img src="/assets/thumb-ld-front.jpg"></a>
<a href="/assets/ld-back.jpg"><img src="/assets/thumb-ld-back.jpg"></a>

## Axe behind her back?

<a href="/assets/poster.jpg"><img src="/assets/thumb-poster.jpg"></a>

<a href="/assets/axe-back.jpg"><img src="/assets/thumb-axe-back.jpg"></a>

I remember the axe being behind her back but at some point this was removed. Thankfully the people at Sony blessed on the BluRay and put it back.

## Other versions of the movie


<a href="/assets/bd-rom.jpg"><img src="/assets/thumb-bd-rom.jpg"></a>
<a href="/assets/dvd.jpg"><img src="/assets/thumb-dvd.jpg"></a>
<a href="/assets/movie-poster.jpg"><img src="/assets/thumb-movie-poster.jpg"></a>
<a href="/assets/poster-cake.jpg"><img src="/assets/thumb-poster-cake.jpg"></a>
<a href="/assets/uk-dvd.jpg"><img src="/assets/thumb-uk-dvd.jpg"></a>

The last one is how I remember the UK PAL VHS copies of the movie
growing up. The American VHS is the same, although in a slip case.

## HD-DVD?

An HD-DVD of the movie showed up on Amazon.com in August 2021. We're waiting for confirmation before committing to this.
