---
Title: 
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

# Keith Selvin: 1989-2021

Keith, we met you under some pretty strange circumstances -- I was
riding a train back from NYC when I noticed the credit and asked
Stevie about it. Within an hour he found you on Facebook, and a few
weeks later we met you at the Anniversary screening. At the Axe-ter
party we drank whiskey and ate veggie burgers. It doesn't feel real
that you're no longer around. We'll miss you bud.

<a href="https://www.forevermissed.com/keithselvin/about"><img src="/assets/keithmax.jpg" style="border: 1px solid lime; -webkit-filter: grayscale(100%); filter: grayscale(100%);"></a>

