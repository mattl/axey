---
Title: Buy the movie
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Buy the movie

Okay, if you haven't seen it yet or you just haven't seen it recently, you can buy the movie in a whole bunch of different ways now.

<img src="/assets/dbd-logo-reversed.png" align="right">I don't recommend buying movie downloads as they are DRM-encumbered and sooner or later you'll want to play them somewhere not permitted. If you must buy a digital copy, [buy it from YouTube](https://www.youtube.com/watch?v=H67O7TXMFwk) which will probably play anywhere YouTube works. I admit I have bought a copy here myself so I can watch the movie in bars and on trains. If YouTube doesn't sell a copy in your country, read on.

### Blu-Ray (BD)

The movie is available on BluRay. This is the best quality version of the movie currently available. [Buy it on Blu-Ray from Amazon](https://www.amazon.com/Married-Murderer-Special-Live-Blu-ray/dp/B0017APPSY).

### DVD

The movie is also available on DVD. Unfortunately neither of the home media disc formats currently sold include much in the way of special features, so you're not missing anything on this six-movie sell-through DVD, and you get a copy of Mixed Nuts which is really good, and the Jim Carrey Medieval Times movie, Cable Guy. [Buy it on DVD from Amazon](https://www.amazon.com/Beverly-Hills-Bewitched-Married-Murderer/dp/B00JSY7566/ref=pd_lpo_sbs_74_t_0?_encoding=UTF8&psc=1&refRID=97RT1AEMH8X3SCP1JP5Y).

### VHS

VHS copies do show up on Amazon all of the time. Maybe you don't have a VCR anymore? I would recommend buying this on VHS anyway, just so you can impress people who come over to your home. This also makes quite an interesting Xmas gift for people in about 2002, believe me. [Buy it on VHS from Amazon third-party sellers](https://www.amazon.com/gp/offer-listing/B00005ALNL/) or even [buy it from eBay](https://www.ebay.com/sch/i?&_nkw=so+i+married+an+axe+murderer+vhs) -- please note, you might wind up getting a UK or Australian PAL format copy of the movie if you're not careful.

### Other formats

The movie was also released on Laserdisc and Video 8 formats in some parts of the world. [See my media page](media) for a look at some of those versions.