---
Title: About this site
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
Img: true
---

## About this site

This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. The title is a bit long winded so we started calling it Axey. Since then we've started calling other movies like this, so there's also [Finny](https://www.imdb.com/title/tt0981227/), [Freedy](https://www.imdb.com/title/tt0309614/) and the TV show, [Elsey](https://www.imdb.com/title/tt0083483/).

All of my content is freely licensed under Creative Commons Attribution-ShareAlike 4.0. That's the same license as Wikipedia, so feel free to add my content to Wikipedia or Wikimedia Commons. I don't claim copyright over any of the photos on this website, including any GIFs. Many of them are taken from random eBay auctions that didn't last very long.

If you want to support the movie, [buy it](buy).

---

### About me

<img src="https://mat.tl/img/mattl.jpg" width="200" alt="Dr. Matt Lee, yesterday" align="right" />

I'm Dr. Matt Lee, aka mattl. You can find [more about me on my homepage](http://mat.tl) or [follow me on Twitter](https://twitter.com/mattl) where I talk rubbish.

I made a movie, [Orang-U: An Ape Goes To College](http://orangumovie.com) which you can watch from the website. There's also a book and a soundtrack.