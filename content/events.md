---
Title: Events
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Events

* We're showing the movie in Boston, August 26th 2018. See [the event page for more details](live)

* [Alamo Drafthouse](https://drafthouse.com/show/so-i-married-an-axe-murderer-movie-party) is showing the movie in July 2018 in a bunch of locations, check the website for your location.

