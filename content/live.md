---
Title: Live at The Haven
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Live at The Haven!

Yep, for one night only we brougth the movie to Boston's only
Scottish bar and restaurant. Come on down, watch the movie and enter
the lifestyle. All events are free, and open to everyone who is part
of the lifestyle.

* You missed it, of course. But you can see [what it was like](/who-is-keith-selvin)

* Trivia, prizes and more!

* Sunday, August 26th 2018, 6pm until 9pm at [The
  Haven](https://thehavenjp.com) in Jamaica Plain. Get there by the 39
  bus, the E-branch, the Orange Line or take a Lyft.

* [Facebook event for movie](https://www.facebook.com/events/436399626849039).

* [Eventbrite](https://www.eventbrite.com/e/the-haven-presents-so-i-married-an-axe-murderer-tickets-48860082823)

### Ax-ter Party

After the movie, come on down to [JJ Foley's Bar and Grille &mdash; 21
Kingston St.](http://www.jjfoleys.com/location) <strong>Don't go to the wrong location</strong>.

* 10pm to 2am.

* Red Line to Downtown Crossing, or live in the <abbr title="St. Elsewhere">Elsey</abbr> lifestyle and take the 39 bus to Forest Hills, then take the 42 bus
  along Washington Street to Dudley(Update, 2021: now Nubian), and then ride the Silver Line
  along the route of the old elevated Orange Line to Temple
  Place. Foley's is a few blocks away. <strong>Do not go to the wrong
  Foleys. Do not get trapped inside a snow globe.</strong>

* Or take a Lyft.

* [Facebook event](https://www.facebook.com/events/2149978818625249/)

* Or just show up. It's a bar.
