---
Title: Plot
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Plot

<p class="tc spoilers"><blink>Warning: spoilers!</blink></p>

Charlie MacKenzie is a beat poet living in San Francisco, and the
older son of two Scottish immigrants (Stuart and May). His day job is
unclear but perhaps he works at the coffee shop Roads where he
performs or works nearby in the City Lights bookstore.

Charlie is unlucky in love, due to his fear of commitment. One of his
most popular poems at the beat poetry night at Roads is "the woman,
woman, woman" poem, in which Charlie tells the crowd why his latest
relationship has failed. Most recently, he broke up with Sherri who
may or may not have stolen his cat. Recently he's also dated Jill who
was unemployed, and therefore to Charlie, in the Mafia. And then
there's Pam who smelled exactly like beef vegetable soup.

Charlie's friend Tony, who is an detective with the San Francisco
Police Department tells him he's paranoid. Then again, Tony believes
the way to look hip while undercover is to dress like a circa-1970s
pimp. He reminds Tony to act normal when hanging out with his parents
the following night, and while driving to meet everyone he stops off
to buy a haggis for his parents at "Meats of the World".

We soon meet Charlie's family -- his brother William who has a pet
eel, and his eccentric parents Stuart and May. They're Scottish and
proud of it. They have a Scottish Wall of Fame, listen to the Bay City
Rollers, drink McEwan's and watch the Scottish soccer team on
television. Charlie has no time for soccer, and helps his mother make
dinner while telling her of his breakup. She shows him an article in
"the paper" -- actually the entertainment magazine, Weekly World News
-- about a mysterious woman who has been marrying men under fake
identities and then killing them on their honeymoons. To Charlie, this
makes some sense because to Charlie, marriage is death.

Charlie soon visits "Meats of the World" again after seeing the owner, Harriet,
in the street, and offers to help her out as his father was a butcher
and he used to work in the store. After work they go on a date, and
later spent the night together.

The next morning, we meet Harriet's sister Rose. She explains that
they live together, although Harriet often comes and goes but always
winds up back with her sister. She warns Charlie to be careful.

As Charlie and Harriet become closer, she tells him of her life and
many of the details seem to match up with the mysterious killer from
the newspaper. At first, Charlie brushes this off but soon becomes
paranoid when she begins to shout the name of one of the dead men in
her sleep. After taking Harriet to meet his parents, he comes
increasingly paranoid and eventually breaks up with Harriet worried
that she may kill or leave him. After several days of feeling sorry
for himself, Charlie is surprised by a call from Tony who says someone
else has turned themselves in for one of the murders and in a fugue of
elation, Charlie rushes to meet Harriet and apologize. She tells him
she loves him, but that he blew it.

Luckily for Charlie, Harriet is a sucker for beat poetry on her roof
and after writing and performing a terrible poem about her, she
welcomes him back. Weeks go by and their relationship seems stronger
than ever, when Charlie proposes to Harriet at his parents wedding
anniversary. Harriet reluctantly agrees, and they soon marry.

Charlie takes Harriet to a secluded mansion for their honeymoon, a
place where many of the original beat poets would stay. At the
mansion, he receives a call from Tony saying that his original lead
was bogus and that he faxed a photo of Harriet to the families of the
three victims and it checks out -- Harriet is the murderer!

Charlie doesn't want to let on that he knows and tries to play it off
as cool, but when the power goes out he begins to lose his cool. Tony
is on his way, but has to charter a plane and commandeer a car to get
to Charlie in time. Back in their room, Charlie admits he knows about
Harriet's husbands and locks her in a closet until Tony can arrive to
arrest her.

At this point, he finds a note apparently written by him *to* Harriet
saying he can't take the pressure and that he's leaving. This is when
the true axe murderer is revealed -- Rose. She doesn't want to lose
her sister, and says that Charlie is no different to the others. It
seems Harriet didn't know this either but is locked in a closet unable
to help Charlie as Rose chases him on the roof of the mansion with an
axe. Tony soon arrives and not knowing the truth, proceeds to put
Harriet in handcuffs and is apparently oblivious to the axe fight on
the roof. Soon he sees the error of his ways and comes to the rescue
of Charlie who has overcome Rose.

The movie ends with Rose being lead away by the police, seemingly
happy that she's been caught finally, while Charlie performs his
"woman, woman, woman" poem for the final time, now changed to admit
that he's no longer afraid of commitment.