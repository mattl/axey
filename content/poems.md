---
Title: Poems
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Poems

These are the four poems we see Charlie perform in the movie. Let me know if you spot any errors, please.

## Woman, woman, woman (original)

Woman...  
Woe-man...  
Whoooa-man!  

She was a thief  
You gotta belief  
She stole my heart and my cat.  

Betty,  
Judy,  
Josie and those hot Pussycats...  

They make me horny,  
Saturday morny...  
Girls of cartoo-ins...  
Won't leave me in ruins...  

I want to to be Betty's Barney.  

Hey Jane...  
Get me off this crazy thing...  

Called love.

## Stevie does woman, woman, woman

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/BsllZWunwDM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## O Butcher Lady (not performed but in his notebook)

O Butcher Lady  
Killer of sheep  
And men  

Untrusting  
Unknowing  
Unloving  
  
This poem sucks  

## Woman, woman, woman (Mrs. X version)

Woman...  
Woe-man...  
Whoooa-man!

We had love not just sex  
Is she Mrs. X?  
I had to run for my life!  

(pauses)

Jane!
Get me off this crazy thing  
Called love

## Bellicose Butcher

Harriet!  
Harri-et!  
Hard hearted harbinger of haggis!  

Beautiful!  
Bemused!  
Bellicose butcher!

Untrusting!  
Unknowing!  
Unlove-d?  

"He wants you back!" 
He screams into the night air  
Like a fireman going to  
A window that has no fire  
Except the passion of his heart...  

I am lonely!  
It's really hard!  
This poem sucks!  

## Harriet

Rose!  
Jailbird!  
Happy in her cage   
No longer full of rage  
She roosts  

Harriet!  
Sweet Harriet!  
You acted cuckoo  
'Cause you thought I would leave you  
Sweet bird  

Harriet!  
Sweet Harriet!  
So knowing!  
So trusting!  
So love-d!  

Harriet!  
Sweet Harriet!  
