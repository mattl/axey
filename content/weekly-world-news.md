---
Title: Weekly World News
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## <img src="/assets/wwn.png" alt="Weekly World News" />

The issue of the Weekly World News used in the movie is a real one, from July 7th 1992. For a number of years in the late 90s and early 2000s, I would regularly subscribe to the magazine, buy McEwans and read 'the paper'.

I have been able to find much of the original issue of the WWN from that particular date and I have reproduced it below. Sadly, the Weekly World News is no longer being printed but it is still published online, but you can buy original uncirculated copies from their website.

## [http://weeklyworldnews.com/](http://weeklyworldnews.com/)

---

<a href='/assets/wwn-0000.jpg'><img src='/assets/thumb-0000.jpg'></a>
<a href='/assets/wwn-0001.jpg'><img src='/assets/thumb-0001.jpg'></a>
<a href='/assets/wwn-0002.jpg'><img src='/assets/thumb-0002.jpg'></a>
<a href='/assets/wwn-0003.jpg'><img src='/assets/thumb-0003.jpg'></a>
<a href='/assets/wwn-0004.jpg'><img src='/assets/thumb-0004.jpg'></a>
<a href='/assets/wwn-0005.jpg'><img src='/assets/thumb-0005.jpg'></a>
<a href='/assets/wwn-0006.jpg'><img src='/assets/thumb-0006.jpg'></a>
<a href='/assets/wwn-0007.jpg'><img src='/assets/thumb-0007.jpg'></a>
<a href='/assets/wwn-0008.jpg'><img src='/assets/thumb-0008.jpg'></a>
<a href='/assets/wwn-0010.jpg'><img src='/assets/thumb-0010.jpg'></a>
<a href='/assets/wwn-0011.jpg'><img src='/assets/thumb-0011.jpg'></a>
<a href='/assets/wwn-0012.jpg'><img src='/assets/thumb-0012.jpg'></a>
<a href='/assets/wwn-0013.jpg'><img src='/assets/thumb-0013.jpg'></a>
<a href='/assets/wwn-0014.jpg'><img src='/assets/thumb-0014.jpg'></a>
<a href='/assets/wwn-0015.jpg'><img src='/assets/thumb-0015.jpg'></a>
<a href='/assets/wwn-0016.jpg'><img src='/assets/thumb-0016.jpg'></a>
<a href='/assets/wwn-0017.jpg'><img src='/assets/thumb-0017.jpg'></a>
<a href='/assets/wwn-0018.jpg'><img src='/assets/thumb-0018.jpg'></a>
<a href='/assets/wwn-0020.jpg'><img src='/assets/thumb-0020.jpg'></a>
<a href='/assets/wwn-0021.jpg'><img src='/assets/thumb-0021.jpg'></a>
<a href='/assets/wwn-0022.jpg'><img src='/assets/thumb-0022.jpg'></a>
<a href='/assets/wwn-0023.jpg'><img src='/assets/thumb-0023.jpg'></a>
<a href='/assets/wwn-0024.jpg'><img src='/assets/thumb-0024.jpg'></a>
<a href='/assets/wwn-0025.jpg'><img src='/assets/thumb-0025.jpg'></a>
<a href='/assets/wwn-0026.jpg'><img src='/assets/thumb-0026.jpg'></a>
<a href='/assets/wwn-0027.jpg'><img src='/assets/thumb-0027.jpg'></a>
<a href='/assets/wwn-0028.jpg'><img src='/assets/thumb-0028.jpg'></a>
<a href='/assets/wwn-0029.jpg'><img src='/assets/thumb-0029.jpg'></a>
<a href='/assets/wwn-0030.jpg'><img src='/assets/thumb-0030.jpg'></a>
<a href='/assets/wwn-0031.jpg'><img src='/assets/thumb-0031.jpg'></a>
<a href='/assets/wwn-0032.jpg'><img src='/assets/thumb-0032.jpg'></a>
<a href='/assets/wwn-0033.jpg'><img src='/assets/thumb-0033.jpg'></a>
<a href='/assets/wwn-0034.jpg'><img src='/assets/thumb-0034.jpg'></a>
<a href='/assets/wwn-0035.jpg'><img src='/assets/thumb-0035.jpg'></a>
<a href='/assets/wwn-0036.jpg'><img src='/assets/thumb-0036.jpg'></a>
<a href='/assets/wwn-0037.jpg'><img src='/assets/thumb-0037.jpg'></a>
<a href='/assets/wwn-0038.jpg'><img src='/assets/thumb-0038.jpg'></a>
<a href='/assets/wwn-0039.jpg'><img src='/assets/thumb-0039.jpg'></a>
<a href='/assets/wwn-0040.jpg'><img src='/assets/thumb-0040.jpg'></a>
<a href='/assets/wwn-0042.jpg'><img src='/assets/thumb-0042.jpg'></a>
<a href='/assets/wwn-0043.jpg'><img src='/assets/thumb-0043.jpg'></a>
<a href='/assets/wwn-0044.jpg'><img src='/assets/thumb-0044.jpg'></a>
<a href='/assets/wwn-0045.jpg'><img src='/assets/thumb-0045.jpg'></a>
<a href='/assets/wwn-0046.jpg'><img src='/assets/thumb-0046.jpg'></a>
<a href='/assets/wwn-0047.jpg'><img src='/assets/thumb-0047.jpg'></a>