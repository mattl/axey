---
Title: Characters
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Characters

### Charlie MacKenzie

A beat poet who possibly works at the coffee shop/bar "Roads" where he also performs. If he's not working there, I'd love to know where he works. In the original screenplay, he works next door at the City Lights bookstore. Maybe he still does, but they don't mention what he does for a living, which can only lead me to suspect he's in the Mafia.

### Harriet Michaels

A butcher. Proprietor of "Meats of the World". Links her own sausage.

### Tony (Anthony) Giardino

A detective with the SFPD. Wishes he was more like the cops you see on TV.

### Rose Michaels

Harriet's sister Rose. Has both Apple Jacks and Froot Loops cereals.

### May MacKenzie

Charlie's Scottish mother. Avid reader of the [Weekly World News](/weekly-world-news). Juices everything now on her Jack Lalanne Juice Tiger.  She has a crush on Tony.

### Stuart MacKenzie

Charlie's conspiracy theory loving vulgar Scottish father. Proud of his Scottish heritage. Likes to drink McEwans and read about the illuminati. 

### William "Head" MacKenzie

Has a head that's like an orange on a toothpick. Seems to have a pet eel. This is is a mistake. Eels are awful.

### Other characters

* Mike Hagerty -- it wouldn't be a Mike Myers film without Mike Hagerty. Which is no problem whatsoever, as Mike Hagerty is awesome.

* Michael Richards -- Michael Richards is this movie.

