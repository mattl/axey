---
Title: Axey fans
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Axey fans

If you want to be on this list, send me email <mattl@cnuk.org> and tell me why you're a fan. First come, first served. Include a URL to your homepage if you want a link.

* \#1 fan forever -- [Keith Selvin](keith-selvin-obituary) (1989-2021)

1) [Matt Lee](http://mat.tl)
2) <small style="font-size: 70%">[Stevie DuBois](http://stevie.boston)</small>
3) [Max Grinnell](http://theurbanologist.com)
4) [Jason Waddleton](http://thehavenjp.com)
5) Eric Bateman 
6) Phil Kerrigan
7) [Alexandra Gallant](http://alexandragallant.com)
8) [Dianne de Guzman](https://www.sfgate.com/local/article/San-Francisco-Married-Axe-Murderer-Mike-Myers-15973927.php?IPID=SFGate-HP-CP-Spotlight)
9) Paul Morales
10) [Dan Gentile](https://twitter.com/Dannosphere/status/1364680786708860928)
11) Julie Tremaine
12) Vanessa Siino Haack
13) Andrea Torrez
14) Cezar Torrez
15) [Summer Siino](http://summersiino.com)
16) Ryan Dougherty aka Scorpion
17) ['Discount' Dave Dwyer](https://discountdave.neocities.org)
18) Alfredo Romero

If you want to be on this list, send me email <mattl@cnuk.org> and tell me why you're a fan. First come, first served. Include a URL to your homepage if you want a link. No homepage? Check out <a href="https://neocities.org">Neocities</a> -- it's free!.
