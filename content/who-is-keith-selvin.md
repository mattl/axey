---
Title: Who is Keith Selvin?
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: index
---

## Who is Keith Selvin?

Update 2021: sadly, Keith died in January 2021 in his early 30s. [Please see our obituary](keith-selvin-obituary). If we get any more information, we'll update this page.

---

Eagle-eyed viewers of the movie will note a credit for one Keith Selvin as
'Young Stuart' -- but who is Keith?

Well, as luck would have it, Keith lives in the same city as me (Boston) and so
when Stevie and I teamed up with Trash Industries and the Shaun Ryder Foundation
to [show the movie at The Haven in
2018](https://thehavenjp.com/2018/08/the-haven-presents-so-i-married-an-axe-murderer/),
Keith came along, saw the movie, signed prizes and bought us a load of drinks.
Absolute legend.

<img src="/assets/poster-output.png" />

<img src="/assets/IMG_1996.jpg" >

Press pack from the movie

<img src="/assets/IMG_1999.jpg" >

Prizes

<img src="/assets/IMG_2001.jpg" >

<img src="/assets/IMG_2002.jpg" >

<img src="/assets/IMG_2003.jpg" >

<img src="/assets/IMG_2004.jpg" >

<img src="/assets/IMG_2005.jpg" >

Keith also sent us over the following images from shooting.

<img src="/assets/keith1.jpg" />

Keith with his father.

<img src="/assets/keith2.jpg" />

Keith on set.

<img src="/assets/keith3.jpg" />

Keith's trailer

<img src="/assets/keith4.jpg" />

Young Stuart, Charlie and Harriet's son.

And here is Keith signing prizes:

<img src="/assets/keithhaven.jpg" />

And here is Keith with the winner of a very large mug, our friend <a href="http://theurbanologist.com">Max Grinnell</a> at the Axe-ter party.

<img src="/assets/keithmax.jpg" />

Thanks to Keith, Max, Jason at The Haven, Pingu at Trash Industries
and X at the Shaun Ryder Foundation for helping us put together a
great night.

