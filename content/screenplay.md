---
Title: Screenplay
Description: This is an unofficial fansite, obviously. It's called "axey.org" because that's what my friends and I call it. 
Author: Dr. Matt Lee
Date: 2020-12-29
Robots: 
Template: screenplay
---

## Screenplay

It's not especially well known, but basically Mike Myers didn't like the original screenplay for the movie and with his friend Neil Mullarkey made a number of changes to the script. Ultimately those changes were accepted but they didn't receive proper credit. I hope to find the version of the screenplay that we see in the movie, but for now here is another version that's been floating around the net for a while.

* [Download PDF](/assets/screenplay.pdf)
* [Download plain text](/assets/screenplay.txt)

Here is the HTML markup converted from the plain text to HTML with Textplay. A useful tool for anyone working with the Fountain format.

<div id="screenplay">

<h3 class="right-transition">FADE IN:</h3>

<h5 class="goldman-slugline">OPEN ON:</h5>

<p class="action">MONTAGE OF VARIOUS SHOTS OF SAN FRANCISCO - DUSK</p>

<p class="action">Over this we hear a recording of Jack Kerouac's poem, San 
Francisco which is accompanied by a BE-BOP trio. Kerouac's 
poetry coincides with the various shots of San Francisco. We 
come to a sign for Jack Kerouac Street. We PAN OVER to "THE 
CITY LIGHTS BOOKSTORE" and continue along to the ALLEYWAY 
where there is a large high-contrast black and white sign 
depicting Jack Kerouac in his famous "I'm looking into the 
distance, having a brilliant thought" pose...</p>

<p class="action">CHARLIE MACKENZIE, in his late twenties, wearing a flannel 
shirt and torn jeans, walks INTO THE FRAME, right in front 
of the picture of Jack Kerouac and inadvertently strikes the 
exact same pose. We PULL BACK to reveal that Charlie has a 
bag of garbage in his right hand, which he deposits in the 
alleyway. We FOLLOW Charlie into...</p>

<h2 class="full-slugline">INT. CITY LIGHTS BOOKSTORE</h2>

<p class="action">We FOLLOW him through the store. By day he is the Assistant 
Manager, by night he is a poet.</p>

<p class="action">A MAN in his fifties, wearing a beret and a goatee is reading, 
Charles Bukowski's, Playing The Piano Like a Percussive 
Instrument, Until Your Fingers Begin To Bleed A Bit.</p>

<p class="action">Charlie takes his place behind the cash register and resumes 
writing in his handsome leather-bound poetry journal.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(sotto)</dd>
<dd class="dialogue">O' SCOTLAND
YOUR SUCKLED TEET OF SHAME</dd>
</dl>

<p class="action">CUSTOMER approaches.</p>

<dl>
<dt class="character">CUSTOMER</dt>
<dd class="dialogue">Do you have the book On The Road by 
Jack Kerouac?</dd>
</dl>

<p class="action">Every day there is a steady stream of tourists who come in 
to get copies of On The Road. Charlie is use to this and 
without looking up he points to a huge, well marked display 
of thousands of copies of On The Road. Another TOURIST COUPLE 
approach.</p>

<dl>
<dt class="character">TOURIST</dt>
<dd class="dialogue">Do you have a copy of On The Road by 
Jack Kerouac?</dd>
</dl>

<p class="action">Again not looking up, Charlie just points.</p>

<dl>
<dt class="character">TOURIST</dt>
<dd class="dialogue">Thanks.</dd>
</dl>

<h2 class="full-slugline">EXT. CITY LIGHTS BOOKSTORE - NIGHT</h2>

<p class="action">Charlie puts the "CLOSED" sign on the door and proceeds to 
walk home.</p>

<h2 class="full-slugline">EXT. SAN FRANCISCO STREETS</h2>

<p class="action">The sights and the sounds of the city are accentuated by the 
BEBOP as he sees life, warts and all. As the streets become 
less populated, he can now hear the sounds of his own 
FOOTSTEPS and, a COUPLE BICKERING. The streets become even 
more deserted. The night is closing in on him. A cat darts 
out from an alleyway and startles him. He quickens his pace. 
RUMBLINGS make him cross the street to avoid the danger. 
Headlights of a slow moving car approach from the distance. 
Charlie, frightened, turns another corner onto:</p>

<h5 class="goldman-slugline">HIS STREET</h5>

<p class="action">He approaches a 3-story Victorian home, in which he has an 
apartment on the second floor, he notices a light on in his 
window. A CRASHING sound from within.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">HANDS</h5>

<p class="action">taking papers out of a desk drawer.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">carefully opening the front door and then gingerly closing 
it. He reaches for a baseball bat in a nearby umbrella stand. 
Sound of BREAKING GLASS from his apartment upstairs.</p>

<h3 class="right-transition">CUT BACK TO:</h3>

<h5 class="goldman-slugline">SHATTERED PICTURE FRAME</h5>

<p class="action">with a photo of Charlie and an angelic blonde.</p>

<h3 class="right-transition">CUT BACK TO:</h3>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">finishing off the last two steps nearing the front door of 
his apartment, bat raised above his head ready to swing.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">THE HANDS</h5>

<p class="action">clasp a jewelry box on the top of the dresser and stuff them 
into a dufflebag; the jewelry is followed by CD's.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">pushing open his apartment door in a mock SWAT maneuver, 
then stealthily stalking toward the sound of the intruder in 
the bedroom. He stubs his toe on a spring loaded doorstop 
making a loud metal VITTSWINGGGG's sound. He freezes, 
terrified.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">THE BEDROOM</h5>

<p class="action">where the HANDS, freeze.</p>

<h3 class="right-transition">CUT BACK TO:</h3>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">Like a coiled jungle cat ready to pounce, waits two beats... 
then springs Samurai style into...</p>

<h5 class="goldman-slugline">THE BEDROOM</h5>

<p class="action">He freezes.</p>

<h5 class="goldman-slugline">REVERSE ANGLE TO REVEAL</h5>

<p class="action">that the HANDS belong to the angelic blonde in the broken 
picture. It's Charlie's girlfriend, SHERRI.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Sherri! What are you doing?</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">I'm leaving you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh, thank God... I thought you were 
robbing our own home, because frankly, 
that's insane. I mean, what could 
you possibly gain by robbing your 
own home? I don't mean to meddle, 
but isn't it better to rob other 
peoples' homes? Start accumulating 
their wealth as opposed to just 
reaccumulating your own wealth.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">That's not funny, Charlie. I'm really 
leaving.</dd>
</dl>

<p class="action">She continues to pack. Charlie tries to unpack her things.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What?! Just because we had a fight 
last night?</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">We've had a fight every night for 
two months. Ever since I brought up 
the subject of marriage, you've found 
fault with everything I do. Why 
couldn't we have gotten married, 
Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(beat)</dd>
<dd class="dialogue">I'm too young to get married.</dd>
<dd class="parenthetical">(begins putting her 
<p class="action">        things back)</dd></p>
<dd class="dialogue">I'm only twenty-nine and a half. We 
love living together.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">It's been two years now. I need 
something more.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">See, Sherri, this is frustrating for 
me, okay. When we first started going 
out I thought we agreed that we 
weren't the sort of people who got 
married.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">That's like saying we're not the 
sort of people who are going to grow 
old. We're not going to fall into 
that "growing old" trap. Face it, 
you've got a problem with commitment, 
Charlie. Take a look at your other 
girlfriends. Every time you get close 
to a commitment there's something 
wrong with them.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, I broke up with them for good 
reasons.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">What about Sandy?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Sandy was an alcoholic.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">No-no-no. You thought she was an 
alcoholic. She just drank more than 
you drank. What about Jill?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">She hated my family.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">You thought she hated your family. 
Nobody hates your family. Everybody 
loves your family. What about Julie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">She smelled like soup.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">What does that mean?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">She smelled exactly like Campbell's 
Beef Vegetable soup. She was dirty, 
physically dirty.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">Well, Charlie, I wonder what you're 
gonna say were my problems? Are you 
gonna tell your friends that I was a 
junkie, that I wasn't supportive 
enough or that I smelled like relish? 
Charlie, I loved you. It could have 
worked out.</dd>
<dd class="parenthetical">(she goes to the door)</dd>
<dd class="dialogue">Think about it.</dd>
</dl>

<p class="action">She leaves.</p>

<h5 class="goldman-slugline">ANGLE ON - THE BROKEN PICTURE</h5>

<h2 class="full-slugline">EXT. SAN FRANCISCO - CHARLIE'S CAR - DUSK</h2>

<p class="action">Charlie and his best friend, TONY SPILETTI, are out for a 
night on the town.</p>

<p class="action">Tony is second generation Italian-American with very 
Mediterranean features. They're listening to Teenage Fan 
Club. They pass Ghierardeli Square.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tony, Teenage Fan Club, they're 
Scottish you know?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Oh.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I had that dream again.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Oh, is that the one where you suspect 
that a fat man in a diaper, on a 
lazy susan has interfered with your 
plans for the evening?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, but I have had that one. No, in 
this one I'm in love...</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Yeah.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">And I say to myself, 'I've finally 
found somebody that I'm truly 
comfortable with.' You know when 
you're so comfortable that you'll 
let them put makeup on you to see 
what you would look like if you were 
a girl. Anyways you know what I do 
in the dream next?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You propose?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">No. I die.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">But Charlie, you're a normal suburban 
guy at heart, from a normal suburban 
family. Didn't you tell me you always 
wanted to get married and have a 
family.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes, but, I'm afraid, okay? There 
are seven main rites of passage in a 
man's life. Birth, first day of 
school, last day of school. Marriage. 
Kids. Retirement. Death. I'm at 
marriage. I'm two rites of passage 
away from death.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I'm sorry, I wasn't listening.</dd>
</dl>

<p class="action">Tony is doing three-sixties, scoping out beauties, when 
suddenly his roving eyes lock on a police car directly behind 
them. He slouches down into his seat.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Christ. It's the cops.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tony, you are a cop.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I know. Isn't it awful? I work with 
those guys. They're assholes.</dd>
</dl>

<p class="action">The police car passes.</p>

<h2 class="full-slugline">INT. SPILETTI'S COFFEE HOUSE - NIGHT</h2>

<p class="action">Tony and Charlie enter. There is a poet on stage. The club 
is full of art tarts and college bohemians. They are greeted 
by the club's owner, GIUSEPPI, an Italian man in his fifties.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Salve zio mio.</dd>
</dl>

<dl>
<dt class="character">UNCLE</dt>
<dd class="dialogue">Allora? Che catzo fai, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hi, Uncle Giuseppi.</dd>
</dl>

<dl>
<dt class="character">UNCLE</dt>
<dd class="dialogue">Tony, come' stai bello il tuo pappa 
e' in galera per la terza volta.</dd>
</dl>

<p class="action">Tony's uncle shows them to a table.</p>

<dl>
<dt class="character">UNCLE</dt>
<dd class="dialogue">I'll have the waitress bring you 
cappuccino.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What did your uncle say?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">He says my Dad's back in jail again.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Ah, I'm sorry, man.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You know, it's funny I don't even 
feel related to my parents anymore. 
I feel like your mom and dad are 
more like my parents. I feel more 
Scottish than Italian.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tony Spiletti, I don't think you 
could get more Italian than that. 
Unless of course your name was Tony 
Italian Guy.</dd>
</dl>

<p class="action">Charlie checks out the girls in the coffee bar.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm so bummed. Sherri was great, 
wasn't she? I'm an asshole, aren't 
I?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Yes.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You've got to help me get through 
this night.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You've just got to get back on the 
horse.</dd>
</dl>

<p class="action">The waitress arrives with two cappuccinos in extremely large 
cups like they have in France.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Waitress, I'm sorry, there seems to 
be a mistake. I ordered the large 
cappuccino.</dd>
</dl>

<p class="action">Two girls at a nearby table, laugh. Charlie and Tony exchange, 
"This could be promising." looks.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(to the girls)</dd>
<dd class="dialogue">Do you think these cups could be 
larger? They're practically bowls.</dd>
</dl>

<p class="action">The girls laugh again.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I feel like I'm having Campbell's 
Cuppuccino.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Join us in a cup of coffee? There's 
enough room?</dd>
</dl>

<dl>
<dt class="character">GIRLS</dt>
<dd class="dialogue">Sure!</dd>
</dl>

<p class="action">The girls come over.</p>

<dl>
<dt class="character">SUSAN</dt>
<dd class="dialogue">My name's Susan and this is June. We 
think you're funny.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">My name's Tony. This is my friend 
Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Look, Tony, I'm going home. See you 
later, girls.</dd>
</dl>

<p class="action">Tony grabs him and pulls him aside.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You really don't understand, do you? 
When a girl comes over to your table 
and says, 'I think you're funny.' It 
means you've pretty much been given 
the keys to the city. Charlie, this 
is big.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Perhaps you've confused me with 
someone who gives a shit. Here's 
what's gonna happen, Tony. We'll end 
up going out with them tonight, maybe 
even home with them. Well go out for 
two months. Soon she'll move in, 
we'll be happy, She'll want more of 
a commitment. I'll be terrified and 
I'll do something to ruin it. Just 
like I did with Sherri.</dd>
</dl>

<p class="action">He leaves. Tony is left with the two girls.</p>

<dl>
<dt class="character">JUNE</dt>
<dd class="dialogue">Poor, guy... He seemed so nice.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(talking, choked up)</dd>
<dd class="dialogue">I just broke up with somebody as 
well. She left me high and dry.</dd>
</dl>

<p class="action">The girls try to comfort him.</p>

<h2 class="full-slugline">INT. CHARLIE'S APARTMENT</h2>

<p class="action">Three quarters of the furnishings and items have disappeared 
with Sherri. Charlie sits dejectedly on the floor over his 
Poetry Journal. He is missing Sherri. We see...</p>

<h5 class="goldman-slugline">CHARLIE'S FACE</h5>

<p class="action">He looks out and is struck by an idea and begins to write.</p>

<h5 class="goldman-slugline">ANGLE ON THE JOURNAL</h5>

<p class="action">            I AM LONELY</p>

<h5 class="goldman-slugline">CHARLIE'S FACE</h5>

<p class="action">Again he looks out, finds his inspiration and continues to 
write</p>

<h5 class="goldman-slugline">IN THE JOURNAL</h5>

<p class="action">        IT'S REALLY HARD</p>

<h5 class="goldman-slugline">CHARLIE'S FACE</h5>

<p class="action">A gentle tear rolls down his left cheek. He pauses, then 
finishes off the stanza.</p>

<h5 class="goldman-slugline">IN THE JOURNAL</h5>

<p class="action">        THIS POEM SUCKS</p>

<p class="action">After the last line he scratches out the entire poem. He 
closes the book and turns on the TV set to CNN to veg out. 
The show is "What's Cooking! With Burt Wolf."</p>

<h2 class="full-slugline">EXT. SAN FRANCISCO STREET</h2>

<p class="action">Charlie is driving in his car. He drives slowly looking for 
an address. Finds it, slips in to a parking spot in front.</p>

<h2 class="full-slugline">EXT. BUTCHER'S SHOP - MEATS OF THE WORLD</h2>

<p class="action">Adorning the front are a "GRAND OPENING" sign and miniature 
flags of the world. Charlie goes inside.</p>

<h2 class="full-slugline">INT. BUTCHER'S SHOP</h2>

<p class="action">It's a small, hip shop selling specialty meats from around 
the world. Charlie looks around. Suddenly, an attractive 
woman in her late twenties, wearing a blood-stained smock 
enters. It is HARRIET MICHAELS. She has a cleaver in one 
hand and something bloody in the other.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(angry)</dd>
<dd class="dialogue">Goddamn shoplifter.</dd>
<dd class="parenthetical">(conscious of Charlie's 
<dd class="dialogue">presence; holding up </dd>
<p class="action">        bloody meat)</dd></p>
<dd class="dialogue">But I got him!</dd>
<dd class="parenthetical">(smiles)</dd>
<dd class="dialogue">You're next.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(backing out the door; 
<p class="action">        terrified)</dd></p>
<dd class="dialogue">I've come at a bad time.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">No stay!</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, no, really... Obviously you've 
got things you have to do. You've 
got to dismember the rest of his 
bloody torso. Dig a makeshift shallow 
grave. Cover the body with quick 
lime. Really so much to do, so little 
time and I'm only in the way here, 
I'm just gonna go. Good luck.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(referring to meat in 
<p class="action">        hand)</dd></p>
<dd class="dialogue">Oh, this! Oh, no, this is what he 
stole. This isn't a piece of him or 
anything. This is Icelandic Shank.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I bet it goes well with a nice 
Chianti. Fittfittfitt.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(laughs)</dd>
<dd class="dialogue">Can I help you?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes. Do you have haggis?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yes, we do. It's over here in our 
Scottish Cuts section. One?</dd>
</dl>

<p class="action">This is a section under glass flying a Scottish flag, with 
haggis and various cuts of Scottish meat.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes! I've never been able to find 
haggis anywhere, except at my parents' 
house. They're Scottish.</dd>
</dl>

<p class="action">Harriet rounds the counter and wraps up the haggis. Behind 
her is the large "PRUSSIAN VENISON" sign.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(ringing up his order)</dd>
<dd class="dialogue">That'll be fifteen, seventy-nine. 
Will there be anything else?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes. I know it's a long shot, but 
you wouldn't by any chance happen to 
have any Prussian Venison?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Now where in the world would I get 
Prussian Venison?</dd>
</dl>

<p class="action">Charlie's charmed.</p>

<h2 class="full-slugline">EXT. SAN FRANCISCO STREET</h2>

<p class="action">Charlie is driving along listening to Kerouac. We absorb the 
flavor of San Francisco as he drives down Lombard Street.</p>

<h2 class="full-slugline">EXT. CHARLIE'S PARENTS' APARTMENT BUILDING - NIGHT</h2>

<p class="action">An old crappie apartment building in San Francisco. Charlie's 
car pulls up. We hear "SATURDAY NIGHT" by the Bay City 
Rollers.</p>

<h2 class="full-slugline">INT. OUTER HALLWAY OF CHARLIE'S PARENTS' APARTMENT - NIGHT</h2>

<p class="action">Charlie approaches a door.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(calling up)</dd>
<dd class="dialogue">Mom, Dad, I'm here.</dd>
</dl>

<dl>
<dt class="character">STUART (O.S.)</dt>
<dd class="dialogue">We're in here, son.</dd>
</dl>

<p class="action">The apartment is a shrine to Scotland. Scottish paraphernalia, 
miniature Scotty dogs, shortbread tins and, on wall, framed 
pictures depict famous Scotsmen, Sean Connery, Jackie Stewart, 
Alexander Graham Bell, James Doohan (Scottie from "Star 
Trek"), Sheena Easton, Billy Connolly.</p>

<h5 class="goldman-slugline">CHARLIE'S POV - AS WE ENTER THE LIVING ROOM</h5>

<p class="action">We see STUART, MAY, TONY, and little WILLIAM, Charlie's 
fourteen year old little brother all singing:</p>

<dl>
<dt class="character">ALL</dt>
<dd class="parenthetical">(singing)</dd>
<dd class="dialogue">S-A-T-U-R-D-A-Y... NIGHT</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="parenthetical">(noticing Charlie)</dd>
<dd class="dialogue">Come give your old man a kiss or 
I'll kick your teeth in.</dd>
</dl>

<p class="action">The group are eating dinner on TV trays. Charlie walks over 
and turns off the record.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Charlie, put on Charlie Pride, would 
ya? Oh, I love Charlie Pride.</dd>
<dd class="parenthetical">(begins singing; in 
<p class="action">        thick Scottish accent)</dd></p>
<dd class="dialogue">HEY, DID YOU HAPPEN TO SEE THE MOST 
BEAUTIFUL GIRL IN THE WORLD...</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">May, shut it.</dd>
</dl>

<p class="action">STUART MACKENZIE is in his late fifties, a butcher, with 
Coke bottle glasses and thick head of black hair. His red-
haired wife, MAY, is in her fifties, attractive with a soft, 
but tough appearance. Little WILLIAM, has a very large head 
and a skinny neck. Like Charlie, he was born in America.</p>

<p class="action">Charlie gives his Mom a hug, his father a kiss.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, William.</dd>
</dl>

<dl>
<dt class="character">WILLIAM</dt>
<dd class="parenthetical">(on his stomach on 
<dd class="dialogue">the floor; watching </dd>
<p class="action">        TV)</dd></p>
<dd class="dialogue">Hey, Charlie.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">SCORES! MAGIC GOAL!</dd>
</dl>

<p class="action">On the television, Stuart's team, Glasgow Celtic, has scored.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Aye &#8209;&#8209; magic.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Let's have a look at the re-play. 
William, move your head. Look at the 
size of that ooy's heed. I'm not 
kidding. It's like an orange on a 
tooth pick.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Stuart, you're going to give the boy 
a complex.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">I'm not kidding. That's a huge 
noggin'. It has it's own weather 
system. It's a virtual planetoid.</dd>
<dd class="parenthetical">(shouting to William)</dd>
<dd class="dialogue">Heed! Move!</dd>
</dl>

<p class="action">We see the re-play of the goal on TV. Tony sits down and May 
brings over a plate of stew and three types of potatoes, 
piled very high.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Is that enough potatoes, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Enough to recreate Devil's Tower in 
"Close Encounters".</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="parenthetical">(sniffs the air)</dd>
<dd class="dialogue">Do I smell haggis?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Aye, you do.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="parenthetical">(taking it)</dd>
<dd class="dialogue">I'll put it in the frig.</dd>
</dl>

<p class="action">Charlie notices Tony reading some papers. He realizes it's 
literature from the Lyndon H. LaRouche Society.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Dad, what are you doing to Tony now? 
Why do you abuse his mind like this?</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">That's the latest report from Lyndon 
H. LaRouche, outlining how the Queen 
and the Rothschilds masterminded the 
Soviet overthrow, so that they could 
reclaim lands they had annexed during 
the Holy Roman Empire.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(goading Charlie)</dd>
<dd class="dialogue">You know a lot of this makes sense.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I think you're suffering from the 
Stockholm Syndrome, where the hostages 
start to relate to their captors.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Listen, Sonny Jim, it's a known fact 
there's a society of the five 
wealthiest people in the world, called 
the Pentaverate, who run everything 
and meet three times a year at a 
secret country mansion in Colorado, 
known as "The Meadows."</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(sarcastic)</dd>
<dd class="dialogue">And that's obviously why we haven't 
heard about it in the newspapers.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="parenthetical">(inappropriately angry 
<p class="action">        &#38; loud)</dd></p>
<dd class="dialogue">That's right. They fuckin' own the 
papers, smartass. And everything 
else. Why do you think Scotland's 
not been able to get independence? 
Because the Queen the Pentavirate 
and those English dome heads in West 
Minster won't have it.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Who are the other members of this 
pentaverate?</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">The Queen, the Rothchilds, the Gettys, 
the Vatican, and Colonel Sanders 
before he went tits up. Oh, I hated 
the Colonel with his wee beady eyes. 
And that smug look on his face.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Dad how can you hate "the Colonel?"</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Because the Colonel puts an addictive 
chemical in it that makes you crave 
it fortnightly.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Interesting... coo-coo</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Would anyone like a juice? Charlie, 
did I tell you, we bought a Juice 
Tiger?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">A Juice Tiger?</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Aye, it's a juicer. It's part of my 
National Enquirer, Garth Brooks diet. 
Would you like potato juice?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Thank you, no.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Sherri's late.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yeah, uh, Sherri and I broke up.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Oh, you didn't. Sherri was the 
daughter your father was never able 
to give me.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm just not ready for marriage. I'm 
twenty-nine and my poems haven't 
even been published yet.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">But it's not just the poetry is it 
son? You're afraid if you get married 
you'll lose your muse. Look at me, I 
was a strapping young butcher, at 
the height of my creative powers. 
When it came to de-boning a side of 
beef, there was nobody that could 
touch me. Then I married your mother. 
And people would still stand in awe 
as I filleted a shoulder of lamb.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Maybe it's just as well not to get 
married, look at the news. Where did 
I put it?</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Heed. Move that melon of yours into 
the bathroom and get the paper for 
your mother.</dd>
</dl>

<p class="action">William gets the National Enquirer and brings it back.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">That's not news, Dad. That's bullshit. 
I wouldn't wipe my ass with that 
paper.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">What are you talking about? It's the 
fifth highest circulating paper in 
the United States, I'll have you 
know.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Oh, here it is. Mrs. X. The Honeymoon 
Murderer. She marries men under fake 
identities, and then murders them. 
She killed some German martial arts 
expert, and some plumber named Ralph 
Elliot. Her whereabouts are unknown.</dd>
</dl>

<p class="action">There's another goal on the TV set.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Scores! Two nil. Magic!</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Ah, beautiful goal. We HOLD on the 
TV set.</dd>
</dl>

<p class="action">Time passes. The TV set</p>

<h5 class="goldman-slugline">CROSS FADES:</h5>

<p class="action">TO THE END OF THE GAME</p>

<p class="action">The two teams are shaking hands. And the final scores chyron 
shows Celtic beating Rangers three nothing. We see Charlie 
and Tony are leaving. Stuart is blind drunk.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="parenthetical">(singing Rod Stewart's 
<p class="action">        song)</dd></p>
<dd class="dialogue">YOU'RE IN MY EYES, YOU'RE IN MY 
DREAMS...
YOU'RE CELTIC, UNITED 
AND BABY I'VE DECIDED...</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Ah, you're steaming.</dd>
</dl>

<p class="action">She meets Charlie and Tony at the door and kisses him good-
bye. She turns to kiss Tony, and holds on the kiss far too 
long.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(pulling away)</dd>
<dd class="dialogue">See you later, Mrs. MacKenzie.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Oh, you've turned into a sexy Italian 
bastard.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">See you later, mom.</dd>
<dd class="parenthetical">(calling out)</dd>
<dd class="dialogue">See you later, Dad.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Fine. Go! You've stayed your hour.</dd>
</dl>

<p class="action">Charlie and Tony leave and enter...</p>

<h5 class="goldman-slugline">THE HALLWAY</h5>

<p class="action">where they find William sitting on the stairs waiting for 
them.</p>

<dl>
<dt class="character">WILLIAM</dt>
<dd class="dialogue">Take me with you.</dd>
</dl>

<h2 class="full-slugline">EXT. MEATS OF THE WORLD - LATE AFTERNOON</h2>

<p class="action">Charlie's drives by and notices Harriet, who's unwinding the 
store awning in Dutch national costume. The banner announces 
"DUTCH WEEK." "MEATS OF THE WORLD SALUTES DUTCH MEAT."</p>

<p class="action">Charlie slows down to look at her. She looks great in her 
little Dutch costume.</p>

<h2 class="full-slugline">INT. CITY LIGHTS BOOKSTORE - DAY</h2>

<p class="action">Charlie is again writing at the counter. Another PERSON 
enters.</p>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Excuse me. You wouldn't happen to 
have...</dd>
</dl>

<p class="action">Charlie again points to the Kerouac section without looking 
up.</p>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Thanks.</dd>
</dl>

<h5 class="goldman-slugline">ON THE PAD</h5>

<p class="action">Charlie writes...</p>

<dl>
<dt class="character">OH MEAT MAID, </dt>
<dd class="dialogue">IF THE CATTLE HAD HAD A CHOICE, THEY WOULD HAVE SLAUGHTERED 
THEMSELVES 
WILLINGLY 
FOR A CHANCE 
TO BE TOUCHED 
BY YOUR FINGERS</dd>
</dl>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">CHARLIE'S FACE</h5>

<p class="action">She's on his mind.</p>

<h2 class="full-slugline">EXT. MEATS OF THE WORLD</h2>

<p class="action">Charlie's car pulls up. The sign reads, "WELSH WEEK" "MEATS 
OF THE WORLD SALUTES WELSH MEATS"</p>

<h2 class="full-slugline">INT. MEATS OF THE WORLD</h2>

<p class="action">The store is very busy. There is a line at the meat counter 
seven people deep. Charlie takes his place at the end of the 
line.</p>

<p class="action">We see a montage of a persons hands chopping a rack of lamb 
into lamb chops, and carving meat with surgical efficiency.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(spotting Charlie in 
<p class="action">        the crowd)</dd></p>
<dd class="dialogue">Oh, hi haggis, right?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">It was a big hit.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(finishing up with a 
<p class="action">        customer)</dd></p>
<dd class="dialogue">I remember you told me you were 
Scottish, but do you really like 
haggis.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No. I think it's repellent in every 
way. In fact, I think most Scottish 
cuisine is based on a dare.</dd>
</dl>

<p class="action">Harriet laughs.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(to the next customer)</dd>
<dd class="dialogue">Can I help you?</dd>
<dd class="parenthetical">(to Charlie)</dd>
<dd class="dialogue">Sorry, I'm really busy.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Look, um, my dad's a butcher, do you 
need a hand?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Well, actually, Yes.</dd>
</dl>

<p class="action">Charlie puts on a very stylish butcher smock and crosses 
behind the counter.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Can you get me four Belgian 
porterhouses? Do you know what a 
porterhouse looks like?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm meat literate.</dd>
</dl>

<p class="action">Time passes we see a montage of Harriet and Charlie serving 
customers. Ending on a customer's POV of Charlie.</p>

<dl>
<dt class="character">CUSTOMER (O.S.)</dt>
<dd class="dialogue">Yes, do you have any fresh blubber?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'll check.</dd>
<dd class="parenthetical">(pause)</dd>
<dd class="dialogue">You want blubber, right?</dd>
</dl>

<dl>
<dt class="character">CUSTOMER</dt>
<dd class="dialogue">Yeah.</dd>
</dl>

<p class="action">We see Charlie's POV of an Eskimo with a "lower forty-eighth" 
accent.</p>

<dl>
<dt class="character">CUSTOMER</dt>
<dd class="dialogue">My parents are coming to town. You 
know how parents are. They'll drive 
you nuts.</dd>
</dl>

<p class="action">The Eskimo exits, there are no customers left.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Look, I'm really grateful. Can I 
offer you some meat as payment? 
Please, help yourself to some meat.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm trying to be a vegetarian.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Trying to be a vegetarian?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yeah, the problem is I really love 
hot-dogs.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I think the meat industry invented 
hot-dogs to stop people from becoming 
vegetarians. There's got to be 
something I can do to repay you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You could take me to a nice romantic 
dinner.</dd>
</dl>

<h2 class="full-slugline">EXT. PIER - NIGHT</h2>

<p class="action">Charlie and Harriet are eating hot-dogs. As Charlie puts the 
relish on, he smells the relish.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(sniffing the relish)</dd>
<dd class="dialogue">This reminds me of my ex-girlfriend.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I hate talking about old 
relationships.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Then let's not and say we did.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(she laughs)</dd>
<dd class="dialogue">That was easy &#8209;&#8209; What a nice guy. 
You've probably never done a mean 
thing in your life.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You'd be surprised.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'd like to hear.</dd>
<dd class="parenthetical">(to his confused look)</dd>
<dd class="dialogue">Name me something bad you've done in 
your life.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Are you kidding me?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">No. Did you ever steal anything? You 
ever hit someone?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, I've been in fights. Let me 
think.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(as Charlie thinks)</dd>
<dd class="dialogue">Not one bad thing, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tell me something bad you've done. 
And it better be bad. I mean, evil.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">How evil?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Really evil.</dd>
<dd class="parenthetical">(thinks)</dd>
<dd class="dialogue">Like how many people have you brutally 
murdered?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">"Brutal" is such a subjective word. 
I mean, what's brutal to one person 
might be totally reasonable to 
another.</dd>
</dl>

<p class="action">Next to them is a German couple, speaking German, looking 
through a coin-operated binocular. He says something which 
causes her to cry.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">This just reminded her of that scene 
in "Brian's Song".</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Actually, he just proposed to her. 
Those are tears of joy.</dd>
</dl>

<p class="action">She lifts her soda to toast them.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Prost.</dd>
</dl>

<p class="action">The man and woman smile and nod.</p>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Danke, Fraulein.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You're very smart. It's a shame I'm 
going to have to destroy you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Do bright women intimidate you?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, not at all.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Really, what do you look for in women 
you date?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(thinks)</dd>
<dd class="dialogue">Well, I know everyone always say 
"sense of humor", but I'd have to go 
with breast size.</dd>
<dd class="parenthetical">(she laughs)</dd>
<dd class="dialogue">How about you? In a guy.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Income of course, and then...</dd>
<dd class="parenthetical">(thinks)</dd>
<dd class="dialogue">...savings.</dd>
</dl>

<p class="action">He smiles at her.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Me likey how you thinkey.</dd>
</dl>

<h2 class="full-slugline">INT. HARRIET'S APARTMENT - NIGHT</h2>

<p class="action">The lights turn on, and then they enter a very bohemian 
apartment. There is artists paraphernalia strewn around. A 
small bar separates the living area from the kitchen. She 
smiles and walks off into the kitchen.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'll make us some tea.</dd>
</dl>

<p class="action">He checks out her apartment. On the wall there is a huge 
poster of the BOARDWALK IN ATLANTIC CITY.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, you know what this apartment 
needs? A really large oversized poster 
of Atlantic City.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I used to live there. That's where I 
had my first supermarket job.</dd>
</dl>

<p class="action">On his way out, he peeks into the bedroom, where he finds a 
bed that is facing neither parallel nor perpendicular with 
the wall. It is just kind of "there".</p>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="parenthetical">(coming into room)</dd>
<dd class="dialogue">I only have chamomile. I hope that's 
all right.</dd>
</dl>

<p class="action">He looks at her and then at the "Oddly-placed" bed.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's North-South.</dd>
<dd class="parenthetical">(to his confused look)</dd>
<dd class="dialogue">For health reasons. See... I had 
this friend, he was a martial arts 
expert. Anyways, he used to sleep 
North-South. I don't know... It's a 
martial arts thing and it just sort 
of became a habit with me.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(walking into living 
<p class="action">        room)</dd></p>
<dd class="dialogue">You know Scotland has it's own martial 
arts. It's called FUCKU. It's mostly 
head butting and kicking people when 
they're on the ground.</dd>
</dl>

<p class="action">Harriet starts laughing. Then so does Charlie. They lean 
into each other. Pretty close. Too close even, and when it 
seems like they're going to kiss, Charlie suddenly gets 
uncomfortable and looks at his watch.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Late?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No. No. Not for me.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Who for then?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Who for then what?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Well, you looked at your watch and 
said it wasn't late for you... I 
wondered who it was late for.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Not me. No, Sir. Not here.</dd>
<dd class="parenthetical">(after a pause; 
<p class="action">        checking watch)</dd></p>
<dd class="dialogue">Maybe it is late.</dd>
</dl>

<p class="action">She gets him his coat. He starts to leave.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Look, the truth is, yes, I had a 
great time, and I'd like to kiss 
you, but if we do kiss, then we'll 
kiss on the couch and if we kiss on 
the couch, then we'll kiss in the 
bedroom, and once you're in the 
bedroom &#8209;&#8209; Well, the thing is, I 
always rush it. And this time I feel 
like maybe I should wait. Maybe we 
should let it build naturally and 
grow, instead of just immediately 
spending the night together.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I want to spend the night together.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(sold)</dd>
<dd class="dialogue">I have no problem with that.</dd>
</dl>

<h5 class="goldman-slugline">THE BEDROOM - MIDDLE OF THE NIGHT</h5>

<p class="action">They are both fast asleep. She is curled up in his arms. 
Suddenly, she begins to speak.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yes! Yes!</dd>
</dl>

<p class="action">Charlie's eyes open. He smiles.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yes Ralph. I will. Ralph.</dd>
</dl>

<p class="action">Charlie's smile fades. He sits up and looks at her. She is 
lying completely still on the bed, her eyes closed, and still 
sleep-talking.</p>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="dialogue">Now now Ralph!</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(waking her)</dd>
<dd class="dialogue">Harriet...? Harriet...?</dd>
<dd class="parenthetical">(as her eyes open)</dd>
<dd class="dialogue">You were having a dream, or...? You 
kept saying the name Ralph.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Ralph?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Ralph. I heard you say it.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(sleepily)</dd>
<dd class="dialogue">That's odd. Just today I was thinking 
about, her. She's a friend.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(starting to leave)</dd>
<dd class="dialogue">Is she nice &#8209;&#8209;? Ralph...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yeah. She's great.</dd>
</dl>

<h5 class="goldman-slugline">DISSOLVE INTO:</h5>

<h2 class="full-slugline">INT. HARRIET'S BEDROOM - MORNING</h2>

<p class="action">Charlie is sleeping alone in the bed, and the sound of RUNNING 
WATER is heard off in the distance. His eyes slowly open, he 
looks around, remembers where he is. He puts on his shorts 
and walks towards the bathroom.</p>

<h2 class="full-slugline">INT. BATHROOM - MORNING</h2>

<p class="action">Through the steam we can just make out Harriet in the shower 
washing her hair. Charlie walks over.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You know... with this drought in 
California total strangers are urged 
to shower together.</dd>
</dl>

<p class="action">He opens the curtain. It's not Harriet. The woman, ROSE, 
calmly looks at him and closes the curtain.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Go away.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh God. I'm sorry. Jesus. Excuse me.</dd>
</dl>

<p class="action">He backs out of the room.</p>

<h2 class="full-slugline">INT. HALLWAY - HARRIET'S APARTMENT</h2>

<p class="action">The door opens and a hurriedly dressed Charlie emerges. Before 
he gets to the door he once again encounters Rose. She's 
completely dressed. Even her hair is dry.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hi. I'm really sorry. I must have 
scared the... I'm Harriet's friend, 
Charlie, and you must be...</dd>
<dd class="parenthetical">(hopefully)</dd>
<dd class="dialogue">Ralph?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I'm Harriet's sister, Rose. And this 
is Harriet's note.</dd>
</dl>

<p class="action">He reaches for it, but she reads it aloud to him.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(reading)</dd>
<dd class="dialogue">'Dear Charlie, I didn't want to wake 
you, make yourself at home, thanks 
for making me smile.' Harriet.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">That's a very nice note.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I'll make you some breakfast.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Gee, I'd love to but I'm running 
late.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">What would you say to blueberry 
pancakes, bacon, fresh squeezed grape 
juice and Kona coffee?</dd>
</dl>

<h2 class="full-slugline">INT. KITCHEN - LATER</h2>

<p class="action">Charlie and Rose sit at the table each eating a bowl of dry 
cereal.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I'm sorry I didn't have any of those 
other things.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, that stuff'll kill you while 
Fruit Loops are light and probably 
reasonably high in Fiber. I like 
Apple Jacks too.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Got 'em.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">So this is your apartment?</dd>
</dl>

<p class="action">Rose starts sketching Charlie.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Yes. She's been here the past three 
months... ever since she came back 
from Miami. I used to visit her 
occasionally. She didn't speak of 
me?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(shakes his head, no)</dd>
<dd class="dialogue">She told me about a martial arts guy 
and there was some discussion about 
Ralph...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">She spoke of them...?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">She spoke of the martial arts guy 
and screamed about Ralph...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(affectionately)</dd>
<dd class="dialogue">Well, you know Harriet.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Actually, I really don't.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(puzzled)</dd>
<dd class="dialogue">But you did have sex with her?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(taken aback)</dd>
<dd class="dialogue">Hello.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Yet you still don't know her.</dd>
<dd class="parenthetical">(contemplates this)</dd>
<dd class="dialogue">See, that's the problem with sex. 
It's not very revealing.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">My, look at the time.</dd>
</dl>

<p class="action">He stands up.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">You should be careful, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I am... usually. I just... You should 
know, this is very unusual that I 
would do this so soon, in this day 
and age particularly, but... We just 
really hit it off. We did. And...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I'm gonna go now. I won't tell Harriet 
that anything happened.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">But... nothing did happen.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Exactly. Or she would be jealous. 
And when she gets jealous, we both 
know what she's capable of.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, we don't. You do, like I said, I 
just met her.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">You'll be okay, Charlie. Just be 
careful.</dd>
</dl>

<p class="action">She leaves. Charlie is baffled.</p>

<h2 class="full-slugline">INT. CITY LIGHTS BOOKSTORE - DAY</h2>

<p class="action">As Charlie walks by, FRED, a lanky customer in his late teens 
is buying a book.</p>

<dl>
<dt class="character">FRED</dt>
<dd class="dialogue">Hey, Charlie. How you doin'?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Good. Good. Look, Fred...</dd>
<dd class="parenthetical">(leaning in)</dd>
<dd class="dialogue">You got a lot of girlfriends, right? 
You know any girls named Ralph?</dd>
</dl>

<dl>
<dt class="character">FRED</dt>
<dd class="dialogue">Ralph? Gee, Charlie. Isn't that a 
guy's name?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, not necessarily, but... Never 
mind. Thanks, Fred.</dd>
</dl>

<p class="action">Charlie catches the store manager, PENNY, on her way into 
her office.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey Penny, I wanted to ask you &#8209;&#8209; 
you know some girls named Ralph, 
right? I mean, that's a girl's name 
also, isn't it?</dd>
</dl>

<dl>
<dt class="character">PENNY</dt>
<dd class="parenthetical">(confused)</dd>
<dd class="dialogue">I don't think so, Charlie... Uh...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(walking away)</dd>
<dd class="dialogue">Forget it. Thanks.</dd>
</dl>

<p class="action">She walks into her office totally confused.</p>

<h2 class="full-slugline">EXT. DOCKSIDE - ALCATRAZ TOUR KIOSK - MAINLAND - DAY</h2>

<p class="action">Tony and Charlie are waiting in line.</p>

<h5 class="goldman-slugline">AERIAL VIEW OF BOAT</h5>

<p class="action">as they travel to the island.</p>

<dl>
<dt class="character">TONY (V.O.)</dt>
<dd class="dialogue">You know I've lived in this city all 
my life and I've never been to 
Alcatraz.</dd>
</dl>

<h5 class="goldman-slugline">ALCATRAZ</h5>

<p class="action">We open on the LOUD BANGING of a CELL DOOR. We find our tour 
group in the holding area. The PARK RANGER is a beefy man in 
his late fifties and talks with emotionless, military 
precision.</p>

<dl>
<dt class="character">PARK RANGER</dt>
<dd class="dialogue">Hello, everyone I'm a park ranger 
and I will be leading you on the 
tour. All the park rangers here at 
Alcatraz were at one time guards, 
myself included. My name is John 
Johnson, but everyone here calls me 
Vicki. Will you please follow me?</dd>
</dl>

<p class="action">They are led out. We see that Alcatraz is a sinister place. 
Cold and unforgiving. The Park Ranger leads them to the center 
of a cell block.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You're glowing, Charlie. The man's 
in love.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Sssh... Stop it. I'm trying to listen.</dd>
</dl>

<dl>
<dt class="character">PARK RANGER</dt>
<dd class="dialogue">This is the main cell block area. 
Home to such famous criminals as Al 
Capone, Micky Cohen, Joseph "Dutch" 
Critzer, and Robert Stroud, the famous 
Bird Man of Alcatraz. Follow me, 
please.</dd>
</dl>

<p class="action">The Park Ranger leads them past the famous visiting rooms, 
the mess hall, all the way to the solitary confinement area.</p>

<h5 class="goldman-slugline">A CELL</h5>

<dl>
<dt class="character">PARK RANGER</dt>
<dd class="dialogue">This is the cell for solitary 
confinement, that over the years has 
come to be known as Times Square.</dd>
</dl>

<p class="action">Tony and Charlie are at the back of the tour group.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">So did you and Harriet?... you know...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(grinning)</dd>
<dd class="dialogue">Sssh I don't want to talk about it.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">With that look, you don't have to 
talk about it. The grin alone could 
get you five to seven years.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tony, get your mind out of the gutter. 
All you need to know is that she's a 
sweet, kind and loving person.</dd>
</dl>

<dl>
<dt class="character">PARK RANGER</dt>
<dd class="dialogue">Now this is something none of the 
other tour guides will tell you. In 
this particular cell block Machine 
Gunn Kelly had, what we call in the 
prison system, a "bitch." And one 
day, in a jealous rage, Kelly took a 
makeshift knife, or "shiv," and cut 
out his "bitch's" eyes.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Look, what can I tell you. I'm 
smitten. I'm in deep smit. I dunno. 
I just don't wanna talk about it, 
because then I start analyzing and 
that's not good for me.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Good. I think that's good. Just let 
it happen.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Exactly. That's what's gonna be 
different this time. Something strange 
happens, let it go. It's not my 
business... Like Ralph. She says 
Ralph in her sleep.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Who's Ralph?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I don't know who Ralph is. Moreover, 
I don't want to know.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Good.</dd>
</dl>

<dl>
<dt class="character">PARK RANGER</dt>
<dd class="dialogue">And as if blinding his "bitch" wasn't 
enough retribution for Kelly, the 
next day he and four other inmates 
took turns pissing into the "bitch's" 
ocular cavity.</dd>
</dl>

<p class="action">Tony and Charlie look at each other. They're a little queasy.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Exactly.</dd>
<dd class="parenthetical">(beat)</dd>
<dd class="dialogue">Tony, I'm happy. Don't let me screw 
this one up.</dd>
</dl>

<h2 class="full-slugline">INT. EL TORO - IN THE MISSION - DAY</h2>

<p class="action">They are eating Bay burrites.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Did you have a nice date last night?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Rose, I don't really &#8209;&#8209;</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">He disturbed me while I was naked in 
the shower this morning.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yeah, he stayed over?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I didn't mind. Charlie and I laughed 
about it over breakfast.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">That's good.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">He said you had great sex last night.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">He did?</dd>
<dd class="parenthetical">(a beat)</dd>
<dd class="dialogue">Yeah.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">He seems really stuck on you. I hope 
for you that it lasts.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Rose he's a sweet, kind and loving 
person. We like each other, but I 
don't want to think any further. 
It's taken me a long time to get 
back to dating, and I want to take 
things real steady this time.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Well, you can trust me not to tell 
him anything.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">He was quite happy not to talk about 
the past.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I did a sketch of him.</dd>
</dl>

<p class="action">Rose shows the sketch to Harriet.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(looking at the picture)</dd>
<dd class="dialogue">That's good.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Think I've caught him?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">The eyes are good.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Charlie really liked it.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's a good likeness.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Boy, I really hope it works out.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Rose, I don't wanna screw this one 
up.</dd>
</dl>

<h2 class="full-slugline">EXT. HARRIET'S APARTMENT BUILDING - DAY</h2>

<p class="action">Charlie enters the building, holding a handful of poetry 
books. He passes a UNIFORMED DELIVERY GUY coming out. The 
guy nods and Charlie nods back.</p>

<h2 class="full-slugline">INT. HARRIET'S APARTMENT BUILDING - CONTINUOUS</h2>

<p class="action">Charlie gets three feet down the hallway. Stops in his tracks 
and heads back to the front door. He opens it and yells to 
the delivery guy:</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, uh... Ralph...?</dd>
</dl>

<dl>
<dt class="character">DELIVERY GUY</dt>
<dd class="parenthetical">(turning around)</dd>
<dd class="dialogue">I'm Gilbert.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Shit.</dd>
</dl>

<h5 class="goldman-slugline">HARRIET'S DOOR - MOMENTS LATER</h5>

<p class="action">She opens the door enough to see that she is wearing only a 
blouse that goes below her hips. She looks fantastic. He 
hands her the poetry books.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(teasing him)</dd>
<dd class="dialogue">Charlie, they're beautiful. I'll put 
them right in water.</dd>
</dl>

<p class="action">He follows her inside and puts the books on the bureau. He 
goes over and kisses her.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You look great.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I was just getting dressed.</dd>
<dd class="parenthetical">(picking up skirt off 
<p class="action">        couch)</dd></p>
<dd class="dialogue">What do you think of this skirt?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Honestly?</dd>
<dd class="parenthetical">(pulls her close)</dd>
<dd class="dialogue">I'd leave it off.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">So then you think I could go to a 
poetry concert like this?</dd>
</dl>

<p class="action">She drops the skirt and stands there. She's fantastic.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Let's forget the poetry concert. 
It's already been nine hours since I 
last made love to you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(smiling; walking 
<p class="action">        away)</dd></p>
<dd class="dialogue">Come on we're meeting your best 
friend. I wanna look good. The second 
I go to the ladies room he's gonna 
tell you what he really thinks of 
me.</dd>
</dl>

<p class="action">He follows her to the bedroom door, constantly trying to 
kiss her.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Come on, Charlie. We have to be there 
in fifteen minutes.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(following her into 
<p class="action">        bedroom)</dd></p>
<dd class="dialogue">Fifteen minutes. Perfect.</dd>
</dl>

<p class="action">She closes the door on his face.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">Maybe later.</dd>
</dl>

<dl>
<dt class="character">ROSE (O.S.)</dt>
<dd class="dialogue">I thought of calling you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(startled)</dd>
<dd class="dialogue">Aaaahhh!</dd>
</dl>

<p class="action">Charlie turns on his heel. Rose has appeared out of nowhere.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">To warn you, Charlie.</dd>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">There are just some things you should 
know, about Harriet.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">About Harriet?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">About her past.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I don't wanna know. I mean, look 
everyone has some skeletons in their 
past. I only care about the future. 
Not the past.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Here's the thing. I may have to tell 
Harriet.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tell her what?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">That we're lovers.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">We're not lovers.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I know, and it's a damn shame.</dd>
</dl>

<p class="action">Harriet walks in the room, fully dressed, and fully dazzling.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I hope I'm not interrupting.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(feeling weird)</dd>
<dd class="dialogue">No, not at all. We were just talking 
about... Rose and I met yesterday, 
so...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">So I heard.</dd>
</dl>

<p class="action">Harriet hugs Rose and then stands right next to her.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">So, don't you think we look alike?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Oh, we do not. Harriet was always 
prettier than me. And a heck of a 
lot more popular. She always had 
boyfriends. The only thing I ever 
got was good grades.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(slightly uncomfortable)</dd>
<dd class="dialogue">Good grades are good.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">She's just being kind. Show Charlie 
one of your photographs, Rose. Rose 
is a great artist.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">No, Harriet. I don't want to. They're 
not good.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You're so modest. If I weren't here 
to brag for you, I just don't know...</dd>
<dd class="parenthetical">(taking out a 
<dd class="dialogue">posterboard from </dd>
<p class="action">        cabinet)</dd></p>
<dd class="dialogue">Show it to him, Rose. Do it.</dd>
</dl>

<p class="action">He turns it over and there is a picture there. A collage of 
unrelated images put together. And it is beautiful.</p>

<p class="action">But it's very abstract. Violent perhaps. Confused definitely. 
He likes it.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">It's beautiful...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Thanks.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What is it?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I dunno.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What do you call it?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I dunno.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">A lot artists don't like to title 
their work. They feel it biases the 
viewer.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">It is titled. It's called "I dunno".</dd>
</dl>

<p class="action">Charlie looks at it again, then at Rose, then at Harriet. 
It's all a little bizarre, but in a funny way he feels for 
Rose. A hidden talented overshadowed by her sister's beauty.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">We should get going, Charlie. Thanks, 
Rose... See you later.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Bye, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Rose, great to see you. We should 
all go out together some time. The 
three of us. That would be great. 
That would be... interesting.</dd>
</dl>

<p class="action">Charlie and Harriet walk out.</p>

<h2 class="full-slugline">EXT. POETRY FESTIVAL - NIGHT</h2>

<p class="action">Charlie and Harriet wait in line with bohemian types and 
poetry lovers from the suburbs, and all walks of life. 
&#60;meta_block&#62;
&#60;meta-single&#62;&#60;key&#62;Directly behind them are TWO OLD LADIES. The marquee reads&#60;/key&#62;&#60;value&#62; &#60;/value&#62;&#60;/meta-single&#62;
&#60;/meta_block&#62;</p>

<h5 class="goldman-slugline">"POETRY FESTIVAL - TONIGHT ALLEN GINSBERG."</h5>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I think you're going to love Alan 
Ginsberg. He's great.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Oh, I know all about him.</dd>
</dl>

<dl>
<dt class="character">TONY (O.S.)</dt>
<dd class="dialogue">Hey Charlie!</dd>
</dl>

<p class="action">Tony is getting out of a cab accompanied by Susan, the girl 
from Spiletti's Coffee House. He approaches Charlie.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Sorry we're late.</dd>
</dl>

<p class="action">Tony throws his arms wide open and hugs one of the Little 
Old Ladies on the other side of Charlie.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You must be Harriet. I've heard a 
lot about you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(to Tony; re: Harriet)</dd>
<dd class="dialogue">This is Harriet.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Oh. Sorry. Of course.</dd>
<dd class="parenthetical">(whispering to Harriet)</dd>
<dd class="dialogue">I apologize. Charlie described you 
as much older. And heavier.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(smiling)</dd>
<dd class="dialogue">Oh, he did...?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Thank you, Tony. This is my best 
friend.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">And this is Susan. Charlie, you 
remember her from Uncle Giuseppi's.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes, I do.</dd>
</dl>

<dl>
<dt class="character">SUSAN</dt>
<dd class="dialogue">You're funny...</dd>
</dl>

<p class="action">Then she GIGGLES. The girls start inside, Tony lags back 
with Charlie.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(whispers to Charlie)</dd>
<dd class="dialogue">I give Susan one night.</dd>
</dl>

<h2 class="full-slugline">INT. POETRY FESTIVAL - NIGHT</h2>

<p class="action">ALLEN GINSBERG is on stage. He is brilliant. Tony, Charlie, 
and Harriet are all amused. Susan is bored stiff. Charlie is 
looking at Tony. Tony glances over at Susan and gives Charlie 
an "Oh, well." look. Then he looks at Harriet and nods in 
approval of her.</p>

<h2 class="full-slugline">EXT. FISHERMAN'S WHARF - NIGHT</h2>

<p class="action">The four of them walk along the wharf. Charlie is at one of 
those arcade games where you throw bean bags at the puppets 
and try and knock them down. Charlie knocks two down.</p>

<dl>
<dt class="character">ARCADE MAN</dt>
<dd class="dialogue">One more and you get your pick.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(to Harriet)</dd>
<dd class="dialogue">You do it.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">No, Charlie. I'm the worst.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Come on, you'll be great...</dd>
</dl>

<p class="action">The arcade man turns around to watch. Harriet winds up and 
throws the bean bag directly into his neck.</p>

<dl>
<dt class="character">ARCADE MAN</dt>
<dd class="dialogue">Hey!</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Sorry... I told you Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, no, you're okay, you're just 
having control problems.</dd>
</dl>

<p class="action">They both start laughing. He puts his arm around her. In the 
b.g. the wounded arcade man is being led away by a co-worker.</p>

<p class="action">They continued down the boardwalk stand in front of a House 
of Horrors.</p>

<p class="action">It looks somewhat run down and Harriet looks questioningly 
at Charlie.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I know this is really, really cheesy, 
but in a way this is one of the places 
in San Francisco I'm most proud of.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yeah, let's go in.</dd>
</dl>

<p class="action">Tony nods agreement. Susan looks bored. They go inside the</p>

<h5 class="goldman-slugline">HOUSE OF HORRORS</h5>

<p class="action">it's as low rent as Charlie described. The "KEEPER OF THE 
THRESHOLD" so described in a poorly written sign, is an 
overweight man in his late twenties, wearing jeans and a 
denim jacket and a little bit of scary makeup. He looks like 
a roadie for the band, KISS. He stands at a podium, smoking 
and reading a paper. As Charlie, Tony, Harriet and Susan 
pass the Threshold Keeper, he takes a casual drag of his 
cigarette, lets out a little smoke and with zero commitment 
utters:</p>

<dl>
<dt class="character">THRESHOLD KEEPER</dt>
<dd class="dialogue">Boo.</dd>
</dl>

<h2 class="full-slugline">INT. WAX MUSEUM - DAY</h2>

<p class="action">Harriet and Charlie enter Bill's Wax Museum. The OWNER of 
the wax museum greets them.</p>

<dl>
<dt class="character">OWNER</dt>
<dd class="dialogue">Hi. I'm Bill, welcome to my wax 
museum.</dd>
</dl>

<p class="action">They walk over to the exhibits. There are exhibits of Abraham 
Lincoln, Michael Jackson and Dolly Parton. As they look more 
closely they notice that the faces are exactly the same as 
Bill's. They laugh.</p>

<h2 class="full-slugline">EXT. STREET - NIGHT</h2>

<p class="action">Pouring rain. THUNDER. Charlie and Harriet, wrapped in each 
others arms, walking through the rain.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I feel so safe with you right now. 
You're never going to leave me, are 
you? I feel like I could be here 
forever.</dd>
</dl>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">TIGHT SHOT OF RAIN HITTING CHARLIE'S PANIC-STRICKEN FACE</h5>

<h3 class="right-transition">MATCH DISSOLVE TO:</h3>

<h5 class="goldman-slugline">THE REFLECTION OF RAIN ON CHARLIE'S PANIC-STRICKEN FACE</h5>

<p class="action">PULL BACK to see Charlie in bed. He lies awake on his side, 
his back up to Harriet's. She is sound asleep. Suddenly:</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(sleeptalking)</dd>
<dd class="dialogue">Ralph! No, Ralph!</dd>
</dl>

<p class="action">Charlie sighs, then just shrugs and tries to fall asleep. 
What can he do.</p>

<h3 class="right-transition">FADE IN:</h3>

<h2 class="full-slugline">INT. CHARLIE'S PARENTS' APARTMENT HALLWAY - NIGHT</h2>

<p class="action">Charlie and Harriet wait outside his parents' door.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, this is it.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It'll be fine.</dd>
</dl>

<p class="action">They enter the door.</p>

<h2 class="full-slugline">INT. CHARLIE'S PARENT'S APARTMENT - NIGHT</h2>

<p class="action">We again move along the hallway. We pass the Scottish wall, 
of fame, Scottie from "Star Trek", Sir Walter Scott, Sir 
Harry Lauder, Sheena Easton, Al Pacino, Billy Connolly, then 
the CAMERA BACKTRACKS to Pacino, where it HOLDS MOMENTARILY.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Mom, Dad, we're here.</dd>
</dl>

<p class="action">May comes up, wearing a fancy country and western outfit.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Ah, Charlie is this the wee Harriet. 
Ah, she's beautiful.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Thank you.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">She's so sweet. I hope you keep her.</dd>
<dd class="parenthetical">(calling)</dd>
<dd class="dialogue">Stuart, come out here. You tube.</dd>
</dl>

<p class="action">When he comes up, Stuart is wearing only a shirt with his 
boxer shorts.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Ah, it's the wee Harriet.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Stuart, put your pants on.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Hold your horses.</dd>
<dd class="parenthetical">(calling to William)</dd>
<dd class="dialogue">Heed! Pants!</dd>
</dl>

<p class="action">William comes around the corner with his pants.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Dad, what's Al Pacino doing on the 
Scottish wall of fame?</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Oh, that's for Tony. So, Charlie 
tells me you're a butcher. Let's 
talk meat.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Dad, no one wants to talk shop. 
Especially butcher shop.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Come here.</dd>
</dl>

<p class="action">Stuart gets him in a half-Nelson.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Ah! Dad, dad I have a back zit, man 
it kills.</dd>
</dl>

<p class="action">Charlie struggles to free himself. Stuart turns to greet 
Harriet. As he reaches out his hand.</p>

<p class="action">Totally instinctively, Harriet grabs Stuart's hand and twists 
it behind his back. Charlie is startled, as his date has 
just gotten Stuart into a Half-Nelson.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(releasing his hand)</dd>
<dd class="dialogue">I'm sorry. I just... You just 
surprised me. I'm sorry.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">I like this one Charlie. She's quite 
a filly.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'm really embarrassed.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Don't be embarrassed about having a 
good strong butcher's grip. Do you 
link your own sausage?</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Oh, ignore him. Come have a look at 
some photos of Charlie when he was a 
wee'n.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh Mom, don't start with the pictures.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Ah, Charlie, lighten up. You've got 
a pickle up your ass.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(whispering to Harriet)</dd>
<dd class="dialogue">I'm gonna use the bathroom. You be 
okay alone with them?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(kissing)</dd>
<dd class="dialogue">Fine. Don't worry about it. Hurry.</dd>
</dl>

<p class="action">They smile as he leaves the room.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Make sure there's paper, Charlie.</dd>
</dl>

<p class="action">Charlie picks up the pace, scared of what he might hear next.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Make sure you leave the seat down.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(shutting her up)</dd>
<dd class="dialogue">Ma, just show her the pictures.</dd>
</dl>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">And light a match.</dd>
</dl>

<dl>
<dt class="character">MAY</dt>
<dd class="parenthetical">(to Harriet)</dd>
<dd class="dialogue">He always leaves the seat up. He's 
gotta learn.</dd>
</dl>

<h2 class="full-slugline">INT. BATHROOM AT PARENTS' - NIGHT</h2>

<p class="action">He closes the door, and shakes his head. What can he do? 
Those are his parents. On the wall opposite the toilet is a 
well-used dart board with pictures of the Queen Mother and 
Colonel Sanders. Hooked to the magazine caddie is a small 
container of darts.</p>

<h2 class="full-slugline">INT. THE LIVING ROOM - NIGHT</h2>

<p class="action">May excitedly shows Harriet family photo albums.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">This is Charlie with his Uncle Ecky. 
He's a policeman in Canada. And our 
cousins Ruth and Jack. He's just got 
a restraining order from his wife. 
She's a lovely girl. This is Billy. 
He's a member of parliament. He 
drinks.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What a nice family you have.</dd>
</dl>

<h5 class="goldman-slugline">CHARLIE IN THE BATHROOM</h5>

<p class="action">He doesn't seem in any hurry to leave either. He listens 
through the door to Harriet enthusiastically looking through 
old photos.</p>

<p class="action">Charlie glances down at a stack of National Enquirers on the 
magazine rack. He flips through a few.</p>

<p class="action">He sees one of the absurd headlines: "ALIEN UFO SEX DIET" 
Charlie shakes his head.</p>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">Charlie was the cutest baby.</dd>
</dl>

<dl>
<dt class="character">STUART (O.S.)</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">You okay in there, Charlie? You didn't 
fall in, did you?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">Jesus...</dd>
</dl>

<p class="action">Charlie then looks down at another article in the Enquirer 
and reads:</p>

<h5 class="goldman-slugline">"WHO'S NEXT FOR MRS. X - THE HONEYMOON KILLER?"</h5>

<p class="action">It is the article about Mrs. X &#8209;&#8209; the axe-murderer who kills 
her husbands on their honeymoons and then marries again under 
a different identity.</p>

<h5 class="goldman-slugline">IN THE LIVING ROOM</h5>

<p class="action">May is quickly flipping through a photo album, pointing out 
pictures of relatives as she goes:</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I can't believe the resemblance 
between you and Charlie, Mrs. 
MacKenzie.</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE IN THE BATHROOM</h2>

<p class="action">With Harriet speaking in the b.g., Charlie continues reading, 
now absorbed in the article about the 3 victims:</p>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">You have the same smile. It's so 
incredible.</dd>
</dl>

<h5 class="goldman-slugline">"VICTIM #1 - THE GERMAN MARTIAL ARTS EXPERT FROM MIAMI"</h5>

<p class="action">"VICTIM #2 - THE LOUNGE SINGER FROM ATLANTIC CITY"</p>

<h5 class="goldman-slugline">"VICTIM #3 - THE SAN FRANCISCO PLUMBER - RALPH ELLIOT"</h5>

<h2 class="full-slugline">INT. CHARLIE'S CAR - NIGHT - C.U. - HARRIET'S FACE</h2>

<p class="action">Sitting in the front seat of Charlie's car, smiling, content, 
a great meal, a great night out with Charlie and a nice 
evening with his parents.</p>

<p class="action">Slowly PAN across the front seat to Charlie. A nervous anxious 
"what the hell am I getting myself into" look on his face.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">So, that was some move you put on my 
Dad, there. Did you study Karate, 
or...?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">No. Not officially. I dated a guy 
for a while who ran a studio.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh, the martial arts expert. The 
north-south guy. Here in San 
Francisco?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Actually, Miami.</dd>
</dl>

<p class="action">He looks straight ahead, trying to act unfazed. But, he's 
very phased &#8209;&#8209; his expression is covered in it.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Was that before Atlantic City, or 
after?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Oh, that was years ago. Atlantic 
City was recent. I didn't care for 
Atlantic City. A town full of gamblers 
and lounge singers.</dd>
</dl>

<p class="action">He keeps driving.</p>

<h2 class="full-slugline">INT. POLICE STATION - DAY</h2>

<p class="action">Charlie walks through the precinct towards Tony's office, 
holding the National Enquirer in his hand.</p>

<dl>
<dt class="character">DESK SERGEANT</dt>
<dd class="dialogue">Hey Charlie!</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Is Tony back there?</dd>
</dl>

<p class="action">The Sergeant nods and Charlie heads back to the office.</p>

<h2 class="full-slugline">INT. POLICE STATION - DAY</h2>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">O.K., Tony. Do you have the K673 
form completed yet, that street vendor 
incident on Powell Street?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(really bummed)</dd>
<dd class="dialogue">Yes, Captain.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Tony, do you mind my saying that you 
seem a little down?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Captain. It's about my work. About 
being a policeman.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Tony, if there's anything wrong, I'm 
here to listen.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I know. And that's what's irritating, 
you're too nice.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Too nice!?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Yes, You're my captain for gods sakes. 
You should be constantly on my case, 
like the captain on Starsky and Hutch. 
Once a week you should routinely 
haul my ass into your office, accuse 
me of being a maverick and complain 
to me that you're sick and tired of 
defending my screwball antics to the 
commissioner.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Well, as you may know, Tony. I don't 
report to a commissioner. I report 
to a committee, some of whom are 
appointed, some elected and the 
remainder co-opted on a bi-annual 
basis. A quorum &#8209;&#8209;</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Police work should be all about 
running around, following up crazy 
hunches that turn out to be right, 
going out on a limb.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Well Tony, I've never seen it that 
way. For me police work is all about 
following procedure and remaining 
accountable to the general public.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(exasperated)</dd>
<dd class="dialogue">Captain! When I joined the police 
force, I thought I was going to be 
Serpico and unfortunately I ended up 
being Toma. I would have settled for 
Beretta.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">That's interesting Tony. I'm perturbed 
that you should be so disillusioned.</dd>
</dl>

<p class="action">Charlie enters.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, Tony, I gotta talk to you.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Oh, hello, Charlie. Look, I'm in the 
way here. You guys probably have 
something you want to talk about, 
and Tony, if you've still got stuff 
you want to sort out, please, you 
know where the suggestion box is.</dd>
</dl>

<p class="action">The Captain exits.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nice guy. Hey, what's up?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I'm having doubts about being a cop 
again. It's not like how it is on 
cop shows. All I do is fill out papers 
and reports.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Let me get this straight, your Captain 
hasn't threatened to have you up on 
charges so fast you won't know what 
hit you?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">No! He's never once said to me that 
he was going to "throw the book at 
me so hard it'll knock my ass from 
here till Tuesday." Anyways what's 
up?</dd>
</dl>

<p class="action">Charlie pulls out the National Enquirer (the one on MRS. X, 
the Honeymoon Killer).</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Have you heard of this case? Mrs. X? 
She murders her husbands on their 
honeymoons and then changes her 
identity and marries again.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I never heard of it. So what?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Curious, that's all. I read about 
it, and...</dd>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">I think I'm dating Mrs. X.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">Two words, Charlie. Get therapy. 
They have doctors that deal 
specifically with this illness.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Everything's adding up, Tony. One of 
the victims was a martial arts expert. 
Last night at dinner, she put a 
martial arts move on my dad.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">There about twenty thousand people 
in San Francisco who are martial 
arts experts. Should I arrest all of 
them too?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">If they also say Ralph in their sleep 
I think it'd be a good start.</dd>
<dd class="parenthetical">(showing him paper)</dd>
<dd class="dialogue">Ralph Elliot. A plumber from San 
Francisco. Missing since his 
honeymoon.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You're just getting scared. Like the 
dream, you feel Harriet could be the 
one, so you start to suspect her of 
things, 'cause deep down you're scared 
that if she is the one, you'll marry, 
and marriage to you is death.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, don't analyze my dreams, okay? 
They're my dreams. Analyze your own 
dreams.</dd>
<dd class="parenthetical">(a beat)</dd>
<dd class="dialogue">It's not a marrying thing, Tony. 
It's a murdering thing.</dd>
<dd class="parenthetical">(showing him paper)</dd>
<dd class="dialogue">Harriet lived in Atlantic City, right? 
Well so did this guy, right around 
the same time she left town.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(reading article)</dd>
<dd class="dialogue">"Larry Leonard, a crooner who made a 
name for himself for being able to 
sing in six different languages the 
song "Only You".</dd>
<dd class="parenthetical">(putting paper down)</dd>
<dd class="dialogue">Does she know the song "Only You?"</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I don't know. It hasn't come up yet.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Charlie, move past it. You're running 
your life by the National Enquirer.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(defensively)</dd>
<dd class="dialogue">What? It's the fifth highest 
circulating newspaper in the United 
States.</dd>
<dd class="parenthetical">(taking paper back)</dd>
<dd class="dialogue">Mrs. X. Please. Look it up.</dd>
</dl>

<h5 class="goldman-slugline">COMPUTER ROOM AT POLICE STATION - MINUTES LATER</h5>

<p class="action">Charlie and Tony are in the back with KATHY, a stocky black 
woman in uniform, who works in the files department.</p>

<dl>
<dt class="character">KATHY</dt>
<dd class="dialogue">There's no record of any deaths. All 
three of these guys were reported 
missing around the time of their 
honeymoon, but so were the wives. No 
pictures of any of the brides. For 
all we know they just picked up and 
moved away.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">And Ralph Elliot, too?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Charlie, you're talking about three 
guys over a seven year span. That's 
hardly news. No deaths. Elopement in 
this state, as of this day, is still 
not illegal.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(re: the article)</dd>
<dd class="dialogue">Yeah well murder is. And this article 
says that these men were murdered by 
the same woman.</dd>
</dl>

<dl>
<dt class="character">KATHY</dt>
<dd class="dialogue">Mr. MacKenzie, we've found that, 
most National Enquirer articles are 
actually based on our own police 
reports. They take the facts and 
fabricate a story around them.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">It's true, Charlie. You gotta realize 
that. I mean, personally, I would 
lie to you, but Kathy... has this 
crazy notion of always telling the 
truth.</dd>
<dd class="parenthetical">(patting his back)</dd>
<dd class="dialogue">You feel better now?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">It guess so. It's just... if I had a 
photo of Harriet, I could show it to 
the relatives or friends of Mrs. X's 
victims to identify her.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Charlie, listen to me! There is no 
Mrs. X! Drop it! Okay?</dd>
</dl>

<h2 class="full-slugline">INT. HALLWAY - HARRIET'S APARTMENT - EVENING</h2>

<p class="action">Charlie knocks on the door. Rose answers.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(thrilled)</dd>
<dd class="dialogue">You're back. But Harriet's not here 
yet.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Maybe I could wait.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Sure. That would be fine.</dd>
</dl>

<p class="action">She then starts to slowly close the door. He props it open 
with his hand.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Inside? I was hoping...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(letting him in)</dd>
<dd class="dialogue">I'm glad you asked. I didn't want to 
be so forward. I mean, if you're 
waiting inside, then you feel 
obligated to entertain me and keep 
up the conversation just to be polite, 
and really your head might be totally 
elsewhere and then there's the chance 
that you would really want to talk 
and it's me who'd be busy, but in an 
attempt not to be rude, I sit there 
and listen to some story that you 
don't really want to tell and I don't 
really have time to hear. You know?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I couldn't agree with you more.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I think about a lot of things.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Look, if you have work to do, you go 
right ahead.</dd>
<dd class="parenthetical">("ah, here's an angle")</dd>
<dd class="dialogue">I mean, to tell you the truth, I'd 
love to see your work.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Okay! What would you like me to do?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, I don't want to see you work. I 
was talking about your work. Your 
photographs. That one that I saw was 
so, wonderful, and...</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Harriet's far more talented than I 
am.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, I'm sure it's so subjective 
anyway and...</dd>
<dd class="parenthetical">(out of patience)</dd>
<dd class="dialogue">Rose, show me your photos.</dd>
</dl>

<h5 class="goldman-slugline">CLOSE ON PHOTOGRAPHS</h5>

<p class="action">There are two kinds. Beautiful travel pictures and very erotic 
black and white portraits of young men and women. All with a 
slight sadomasochistic quality. At the bottom of every photo 
is says: "Seasons Greetings".</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey, these are some interesting photos 
here. Very impressive. Nice shots of 
Sauselito and... some good bondage 
shots. A lot of people wouldn't think 
to mix the two subjects, but they're 
really a natural together.</dd>
<dd class="parenthetical">(new thought)</dd>
<dd class="dialogue">Hey, you wouldn't happen to have any 
pictures of Harriet by chance, would 
you?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(re: her cards)</dd>
<dd class="dialogue">Well, I don't think she'd wanna do 
this sort of...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, no, not that. Just, in general 
some photos. Any little snapshot 
would do.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I doubt I'd have any. Harriet hates 
being photographed.</dd>
</dl>

<p class="action">The sound of a key in the door as Harriet enters the 
apartment.</p>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="dialogue">Rose &#8209;&#8209; did I see Charlie's car out 
in front?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">We're in here, Harriet.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(walking in)</dd>
<dd class="dialogue">What are you guys doing?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(covering up)</dd>
<dd class="dialogue">Oh, nothing. Just looking through 
some of Rose's work.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">...Charlie wanted a photo of you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">And that. That too.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Why of me, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, sentimental reasons. Something 
to remind me of you when we're not 
together.</dd>
</dl>

<p class="action">She takes him in her arms and gives him a knee buckling kiss.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">There, can you remember that?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Okay, it's just, I was gonna give 
one to my parents, too, and...</dd>
<dd class="parenthetical">(getting nowhere)</dd>
<dd class="dialogue">Another time would be fine. It's 
hardly a matter of life and death.</dd>
</dl>

<h5 class="goldman-slugline">TV SET - PLAYING THE EVENING NEWS</h5>

<dl>
<dt class="character">NEWS ANCHORMAN</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">In the news tonight, regarding a 
Beverly Hills Jeweler, Morris Cohan, 
who died last week, police are now 
suspecting that Morris's partner, 
Lawrence Sachs, may have murdered 
him with an untraceable poison.</dd>
</dl>

<p class="action">Reveal: we are in...</p>

<h2 class="full-slugline">INT. CHARLIE'S BEDROOM - NIGHT</h2>

<p class="action">Charlie is on a Stair Master, as Harriet walks in wearing a 
robe. The TV is on in the b.g.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Where you been?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Downstairs. I have a surprise for 
you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Great. I just wanna do a quick twenty 
minutes on the Stair Master before 
bed.</dd>
</dl>

<p class="action">Harriet drops her robe, and from over her shoulder we see 
that Charlie prefers what he sees to working out.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'll do forty tomorrow.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(getting into bed)</dd>
<dd class="dialogue">I got something much healthier for 
you than that.</dd>
</dl>

<p class="action">She pulls out a milkshake from behind her back.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What is it?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's a health shake. Eggs, malt, 
cinnamon, oranges. It's great. I 
mixed it up downstairs.</dd>
</dl>

<h5 class="goldman-slugline">THE TV SET</h5>

<p class="action">continues on about poisons and poisoners. Charlie glances at 
it.</p>

<dl>
<dt class="character">NEWS ANCHORMAN</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Poisoning has become the second 
leading method of murder in recent 
years, due to...</dd>
</dl>

<p class="action">Charlie watches the TV, looking a bit disturbed. Harriet 
offers him the shake.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh, look, I'm full. Dinner and... 
No...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You'll like it Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, really, thanks.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(putting it up to his 
<p class="action">        lips)</dd></p>
<dd class="dialogue">You won't try it. I spent twenty 
minutes making it.</dd>
</dl>

<p class="action">He takes it. Lifts it to his mouth... then puts it on the 
table.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(sniffing it)</dd>
<dd class="dialogue">Ummm. Smells good. Maybe I'll take 
some to the office tomorrow.</dd>
<dd class="parenthetical">(running into bathroom)</dd>
<dd class="dialogue">I'm gonna brush my teeth. Be right 
back.</dd>
</dl>

<p class="action">Charlie goes into the bathroom.</p>

<h5 class="goldman-slugline">CHARLIE'S BATHROOM</h5>

<p class="action">Harriet comes into the bathroom and lays the empty glass 
down on the counter.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'm gonna take a quick shower.</dd>
</dl>

<p class="action">Charlie notices the empty glass on the counter.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Harriet, where did the shake go?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What do you care? I drank it.</dd>
<dd class="parenthetical">(getting into shower)</dd>
<dd class="dialogue">You could have at least tried it. 
You make me feel bad sometimes, 
Charlie. I don't know why.</dd>
</dl>

<p class="action">With her in the shower, he sneaks back into the bedroom and 
checks the trash can. Nothing. Then he runs around the bed 
to the other trash can. Nothing.</p>

<p class="action">He looks thoroughly confused as she enter the bedroom, wearing 
a towel. She takes the towel off as she slips underneath the 
covers. He gets into bed next to her. She gives him a kiss.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Sorry. I'm a little sensitive. You 
didn't want to drink my milkshake. 
So what &#8209;&#8209; right?</dd>
</dl>

<dl>
<dt class="character">NEWS ANCHORMAN</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Regarding the murder between the two 
partners, we talked to Toxicologist 
Dr. Show on the issue.</dd>
</dl>

<p class="action">Charlie and Harriet are watching the news show. DOCTOR SHOW 
is patched in via the Anchorman's close circuit TV.</p>

<dl>
<dt class="character">NEWS ANCHORMAN</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Doctor, is it possible that one could 
be poisoned with no trace at all?</dd>
</dl>

<dl>
<dt class="character">DOCTOR SHOW</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Certainly. There are plants that 
grow very commonly in our own backyard 
that could easily be fermented into 
poison. Take for instance the...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(getting nervous; 
<p class="action">        blocking out TV)</dd></p>
<dd class="dialogue">Harriet, why don't we shut the light 
off.</dd>
</dl>

<dl>
<dt class="character">NEWS ANCHORMAN</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Really? And how easy it that to do?</dd>
</dl>

<dl>
<dt class="character">DOCTOR</dt>
<dd class="parenthetical">(ON TV)</dd>
<dd class="dialogue">Scarily enough, quite simple. You 
merely take the...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(blocking out the TV 
<p class="action">        again)</dd></p>
<dd class="dialogue">Maybe we should turn the light back 
on. Yeah that's better.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Charlie, what's the matter?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nothing.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Charlie...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, it's just...</dd>
<dd class="parenthetical">(re: the TV)</dd>
<dd class="dialogue">The TV. You can't even watch the 
news these days without getting 
depressed.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I know, Charlie. And it's not just 
that. Look at the things people are 
doing. Partners killing each other... 
I mean, you hear a story like that, 
and... who can you really trust these 
days?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What do you mean?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's like, have you ever stood with 
someone at the edge of a cliff, or 
the edge of a subway platform, and 
you think, just for a split second, 
"What if I pushed him?"</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well, I don't really take the subway 
ever, so...</dd>
</dl>

<p class="action">Charlie turns over on his side, she cuddles up behind him.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'm just making a point of how many 
times we trust people with our lives. 
I mean, look at us. If you didn't 
trust me, you would never be able to 
fall asleep.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Why do you say that?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Look at you, you're sleeping. Look 
how vulnerable you are. I mean, I 
could do anything at that point.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(nervous)</dd>
<dd class="dialogue">What could you do?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(sweet and innocent)</dd>
<dd class="dialogue">Anything. You're lying on your side, 
asleep, I could... stick a needle in 
your ear.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(grabbing his ear at 
<p class="action">        the thought)</dd></p>
<dd class="dialogue">Aahhh!</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'm just making a point of what a 
good relationship we have. Goodnight, 
sweetheart.</dd>
</dl>

<p class="action">He looks very uneasy. She kisses him and shuts off the light. 
The moon gives the room an eerie glow.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Well, good night.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Good night.</dd>
</dl>

<p class="action">She doesn't close her eyes. He's scared to close his. Pause.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Well... good night.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(smiling)</dd>
<dd class="dialogue">Good night.</dd>
</dl>

<p class="action">They both look over at each other. She closes her eyes. He 
takes a deep breath and then closes his eyes.</p>

<p class="action">And covers his ear with his hand.</p>

<h2 class="full-slugline">INT. BART PLATFORM - DAY</h2>

<p class="action">Charlie is on the crowded platform. Next to him is an old 
lady with a lot of shopping bags. Three kids on skateboards 
whiz by and accidentally knock bags out of her hands. Cat 
toys and cans of cat food go everywhere. Charlie bends down 
and starts to help her gather her stuff.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">Thank you very much, young man. I've 
gotta get all this stuff back to my 
children.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Your children?</dd>
</dl>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">When I say my children I mean my 
cats. You see my children moved out 
years ago, so all I've got is my 
cats. I have over one hundred of 
them.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">That's a lot of cats.</dd>
</dl>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="dialogue">Charlie.</dd>
</dl>

<p class="action">Charlie looks up and sees Harriet waving to him from the 
subway stairs. He waves back and motions. "I'll be there in 
a second", and continues to help the old lady. She watches 
from the stairs.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">You see this red toy? That's for the 
Captain, he's finicky. and this blue 
one? That's for Marco Polo.</dd>
</dl>

<p class="action">Two train headlights are seen off in the distance.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Do you have a name for all of your 
cats?</dd>
</dl>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">Oh, yes.</dd>
</dl>

<p class="action">Charlie glances over at Harriet, who slowly makes her way 
down the platform towards him.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">Let me see! There's Winston Churchill, 
Reda Sovine, Thomas Edison, Andrew 
Carnegie...</dd>
</dl>

<p class="action">The train is getting closer and closer, and so is Harriet.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">...He was Scottish.</dd>
</dl>

<p class="action">Harriet moves forward a step, Charlie moves back a step.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">Wasn't he Irish?</dd>
</dl>

<p class="action">As Harriet seems to get closer Charlie continues to back up, 
picking up cat toys. Charlie realizes he has no where else 
to turn. so he side steps down the platform, never stopping 
his conversation with the lady.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Actually he was Scottish. Trust me, 
I know these things.</dd>
</dl>

<p class="action">Harriet is moving in on him. Charlie steadily makes his way 
down the platform, feigning accidentally kicking cat food 
down the platform. The old lady is unsure what is going on, 
she tries to keep up with him.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">Now that you say it, he was Scotch.</dd>
</dl>

<p class="action">Charlie runs out of platform. Harriet is very close to him. 
The train is closer, so is Harriet. Charlie lets out a scream.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Noooooo!</dd>
</dl>

<p class="action">Charlie is standing at the edge of the platform, Harriet is 
a good six or seven feet away as the train passes by. Charlie 
is safe. People are all staring at Charlie curiously, 
including Harriet and the old lady. Charlie is embarrassed.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(embarrassed)</dd>
<dd class="dialogue">Nooooooo, Scotch is a drink. Scots 
are a people. Sorry, that just always 
bugged me.</dd>
</dl>

<p class="action">No one knows what is going on.</p>

<dl>
<dt class="character">LADY</dt>
<dd class="dialogue">I'm sorry, I didn't know it meant so 
much to you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hi, Harriet.</dd>
</dl>

<h2 class="full-slugline">EXT. SAN FRANCISCO CHRONICLE BUILDING - DAY</h2>

<h5 class="goldman-slugline">INT. CHRONICLE ANNOUNCEMENTS DESK.</h5>

<dl>
<dt class="character">WE SEE A LONG DESK WITH DIFFERENT SIGNS THAT READ; BIRTHS, </dt>
<dd class="dialogue">DEATHS, AND MARRIAGES.</dd>
</dl>

<p class="action">We find Charlie at the marriage counter.</p>

<dl>
<dt class="character">ASSISTANT</dt>
<dd class="dialogue">Yes, Sir, can I help you?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'd like to put in an announcement 
of my parents forty-fifth wedding 
anniversary.</dd>
</dl>

<dl>
<dt class="character">ASSISTANT</dt>
<dd class="dialogue">Sure, it's $4.50 per word, and you've 
got a choice of standard or bold.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Bold, and here, I've written it out.</dd>
</dl>

<p class="action">Charlie looks over to the deaths counter. He overhears two 
obituary assistants having a conversation.</p>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">Hi, Frank, busy week?</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #2</dt>
<dd class="dialogue">I've only got two. It's dead around 
here.</dd>
</dl>

<p class="action">Both assistants laugh. Charlie is mildly bemused.</p>

<dl>
<dt class="character">OBITUARY ASSISTANT #2</dt>
<dd class="dialogue">Well, I've got this one guy, a 
tourist. He had a heart attack on a 
cable car.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">Looks like he left his heart in San 
Francisco.</dd>
</dl>

<dl>
<dt class="character">MARRIAGE ASSISTANT</dt>
<dd class="dialogue">Hey, that's a real person you're 
talking about.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">You're right, I'm sorry.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #2</dt>
<dd class="dialogue">Well, there's this other guy Elliot, 
Ralph. Plumber, disappeared four 
months ago. Body found in a sewer.</dd>
<dd class="parenthetical">(pause)</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="parenthetical">(despite himself)</dd>
<dd class="dialogue">I guess he took his work too 
seriously, and his life went down 
the drain.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Did they mention anything about his 
wife?</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="parenthetical">(crest fallen)</dd>
<dd class="dialogue">You're right, I feel bad. Point taken. 
I'm mean, these are real people we're 
talking about.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, I'm serious. Did he mention the 
wife?</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">You made your point. I was wrong to 
make a joke about a person's life.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I really want to know about his wife.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="parenthetical">(crying and shouting)</dd>
<dd class="dialogue">O.K., you win. I'm a bad, bad person.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #2</dt>
<dd class="dialogue">Frank take it easy.</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">No, he's right!</dd>
<dd class="parenthetical">(pounding his head 
<p class="action">        with his fists)</dd></p>
<dd class="dialogue">I'm for shit, I'm one insensitive 
asshole.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Is there any mention of the wife? At 
all?</dd>
</dl>

<dl>
<dt class="character">OBITUARY ASSISTANT #1</dt>
<dd class="dialogue">NO! THERE'S NO MENTION OF THE WIFE! 
YOU HAPPY!?</dd>
</dl>

<p class="action">Charlie exits.</p>

<h2 class="full-slugline">EXT. CHRONICLE ANNOUNCEMENT OFFICE - DAY</h2>

<p class="action">Charlie stands outside the announcement office, terrified.</p>

<h2 class="full-slugline">INT. MEATS OF THE WORLD</h2>

<p class="action">Harriet is talking to a CUSTOMER.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Hi.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm sorry.</dd>
<dd class="parenthetical">(beat)</dd>
<dd class="dialogue">I think you're a terrific woman.</dd>
<dd class="parenthetical">(beat)</dd>
<dd class="dialogue">I just don't think we should see 
each other anymore.</dd>
</dl>

<p class="action">She moves around to Charlie. She lifts his chin so that he 
is looking directly into her eyes.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Why not? And tell me the truth.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">The truth. Okay. The truth is...</dd>
</dl>

<p class="action">She is so close to him, and so very beautiful, it's 
distracting.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">The truth is... I'm afraid that you 
are...</dd>
<dd class="parenthetical">(he can't)</dd>
<dd class="dialogue">You're going to laugh.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I don't think so.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Okay... the truth is that I'm afraid 
you're going to ki... leave me.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'm going to "cleave you?" What does 
that mean?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Leave me. Not "cleave me." Reject 
me. And so I decided to take matters 
into my own hands and get it over 
with by...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Rejecting me.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(he feels awful)</dd>
<dd class="dialogue">Purely preventive... It's not anything 
you've done.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I know that... So why are you leaving 
me?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(heartbroken)</dd>
<dd class="dialogue">Harriet, maybe I'm not meant to be 
in a relationship.</dd>
</dl>

<p class="action">A single tear runs down her cheek. She brushes it away 
quickly.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I never wanted to hurt you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You haven't. At least you left early 
on.</dd>
<dd class="parenthetical">(she's crying)</dd>
<dd class="dialogue">So, that's it, then. I've got a lot 
of work to do.</dd>
<dd class="parenthetical">(to Customer)</dd>
<dd class="dialogue">Now, where were we?</dd>
</dl>

<p class="action">Charlie goes.</p>

<h2 class="full-slugline">INT. SPILETTI'S COFFEE HOUSE - NIGHT</h2>

<p class="action">Charlie lies on the bar head down. Tony rushes in, looks 
around and sees Charlie.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(without lifting head)</dd>
<dd class="dialogue">Two hours and four minutes. Tony, I 
need you, and two hours and four 
minutes later you show up.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Sorry. I know it was irresponsible 
to stay at the drug bust until it 
was over, but... What happened?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(slowly sitting up)</dd>
<dd class="dialogue">I'm gonna tell you, but when I do, 
just say nothing. Don't judge me. 
Just be my friend. Okay?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Fine. Okay.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I broke up with Harriet.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You're an asshole.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What's your point?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I'm sorry, I just... why?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tony, she's a killer. The... 
everything.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">But nothing's proven. The only thing 
you're actually sure she did so far 
is she's treated you like a King.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I dunno, Tony, I just...</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Besides, everyone has something going 
on with them. I mean, you can't find 
everything in one person. I mean, 
she's bright, she's funny, she's 
independent. So maybe, and it's really 
just a maybe, she kills her husbands. 
Marriage is give and take, Man. You 
take the good with the bad.</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE'S BEDROOM - MIDDLE OF THE NIGHT</h2>

<p class="action">Charlie lies in bed. He's writing in his journal. He stares 
out into space. Inspired, he writes...</p>

<h5 class="goldman-slugline">ANGLE ON THE JOURNAL</h5>

<dl>
<dt class="character">DON'T BE DISILLUSIONED BY THE SCOTTISH SON AS HE FLIES, IN </dt>
<dd class="dialogue">BAT-LIKE UNISON</dd>
</dl>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">pauses a moment to reflect, then writes...</p>

<h5 class="goldman-slugline">ANGLE ON THE JOURNAL</h5>

<dl>
<dt class="character">UNTRUST-ING </dt>
<dd class="dialogue">UNKNOW-ING 
UNLOV-ING</dd>
</dl>

<h5 class="goldman-slugline">CHARLIE</h5>

<p class="action">Thinks of something else and writes...</p>

<h5 class="goldman-slugline">ANGLE ON THE JOURNAL</h5>

<p class="action">THIS POEM SUCKS</p>

<p class="action">His hand reaches across and scratches it out.</p>

<h2 class="full-slugline">EXT. HAIGHT-ASHBURY STREET - DAY</h2>

<p class="action">Charlie is exiting a vintage record store. Suddenly he finds 
himself face to face with Sherri. She's accompanied by a 
handsome young man.</p>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">Hey, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hi. How're you doing.</dd>
<dd class="parenthetical">(he glances at her 
<p class="action">        friend)</dd></p>
<dd class="dialogue">Good, huh?</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">I'm okay. This is Michael. Michael, 
this is Charlie MacKenzie.</dd>
</dl>

<dl>
<dt class="character">YOUNG GUY</dt>
<dd class="dialogue">I know. Why don't you two talk. I'm 
going over there to buy some 
magazines.</dd>
</dl>

<p class="action">He walks over to a magazine stand.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">That good looking and he can read!</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">I'm teaching him. I heard you have a 
new girlfriend.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">We broke up. There were problems.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">Problems?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Difficulties.</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="dialogue">Let me guess...</dd>
<dd class="parenthetical">(smiles)</dd>
<dd class="dialogue">She's a murderer.</dd>
</dl>

<p class="action">For a moment, Charlie is too stunned to respond. Then...</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Why did you just say that?</dd>
</dl>

<dl>
<dt class="character">SHERRI</dt>
<dd class="parenthetical">(laughs)</dd>
<dd class="dialogue">What else is left?</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE'S BEDROOM - EARLY EVENING</h2>

<p class="action">He's on the Stair Master, stepping very lethargically. The 
TELEPHONE RINGS: He goes to answer.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hello...</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(through phone)</dd>
<dd class="dialogue">Not that it matters anymore, but I 
thought you should know &#8209;&#8209; someone 
just turned themselves in for the 
murder of Ralph Elliot.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Really? Did she confess to the other 
murders?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Just the plumber so far, but she'll 
come along.</dd>
<dd class="parenthetical">(after a pause)</dd>
<dd class="dialogue">A little old lady from Pacific 
Heights. Said he overcharged her on 
a leaking sink.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Really. Leaky sink, huh?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Anyway, crime to stop. Gotta go. 
I'll catch you later.</dd>
</dl>

<p class="action">Tony hangs up. Charlie stops pedaling on the bike. Now he 
really feels like shit. Harriet's not a killer. Sherri's not 
a cheater.</p>

<p class="action">He races out of the bedroom.</p>

<p class="action">Moments later he appears, puts on a pair of pants over his 
exercise shorts, then races out the door again.</p>

<h2 class="full-slugline">EXT./INT. CHARLIE'S CAR - EARLY EVENING</h2>

<p class="action">Charlie races along towards Harriet's house.</p>

<h2 class="full-slugline">EXT. HARRIET'S APARTMENT DOOR - DAY</h2>

<p class="action">He races up to the door and starts to bang and knock and 
ring...</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(through door)</dd>
<dd class="dialogue">Harriet, it's me, Charlie.</dd>
</dl>

<dl>
<dt class="character">HARRIET (O.S.)</dt>
<dd class="dialogue">Go away, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I've gotta talk to you, cause I miss 
you, and I made a mistake... and if 
you give me another chance I'll 
change. I will. I promise. I'll get 
help, or therapy, or... Yeah, that'll 
be great. Therapy. Even twice a week. 
I'll check with my insurance to see 
if I'm covered, but forget that. 
Harriet...</dd>
</dl>

<p class="action">The chain opens on the door.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You really hurt me.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'll make it up to you, can we at 
least talk.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Sure, talk.</dd>
</dl>

<p class="action">Rose steps up behind Charlie.</p>

<dl>
<dt class="character">ROSE (O.S.)</dt>
<dd class="dialogue">Hi, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">AAAhhhhhh.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(as she now proceeds 
<dd class="dialogue">to be let in by </dd>
<p class="action">        Harriet)</dd></p>
<dd class="dialogue">Trust your first instincts, Charlie. 
You never do. It's your big mistake. 
That and the haircut.</dd>
</dl>

<p class="action">Once again, baffled by Rose, Charlie touches his hair, shakes 
it off and looks Harriet right in the eye.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I don't want to lose you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You didn't lose me. You rejected me.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm unrejecting you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">How do I know you won't reject me 
again?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I love you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(after a long pause)</dd>
<dd class="dialogue">I love you. But you blew it, Charlie, 
you blew it.</dd>
</dl>

<p class="action">She goes into the house. Charlie stands there dejected. He 
knows he's blown it.</p>

<h2 class="full-slugline">INT. HARRIET'S APARTMENT - NIGHT</h2>

<p class="action">Harriet is doing a load of laundry consisting of bloodied 
work clothes. Suddenly she can hear the sound of MUSIC, very 
loudly.</p>

<p class="action">Annoyed, she goes out her front door to tell her neighbors 
off. Just as she's about to knock on the door, she realizes 
it's not the source of the music. At that moment her neighbor, 
who is a STEWARDESS, comes out in nightclothes.</p>

<dl>
<dt class="character">STEWARDESS</dt>
<dd class="dialogue">I don't mean to be a pain, but I'm a 
stewardess, and I have an early flight 
out in the morning. Can you please 
keep your music down?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I thought it was coming from here.</dd>
</dl>

<dl>
<dt class="character">STEWARDESS</dt>
<dd class="dialogue">But someone keeps shouting your name 
over and over.</dd>
</dl>

<p class="action">Puzzled, Harriet rushes back to her own balcony.</p>

<h2 class="full-slugline">EXT. HARRIET'S APARTMENT - BALCONY - NIGHT</h2>

<p class="action">Harriet rushes out and smiles as she sees the source of the 
noise. Charlie serenades Harriet in the street below, 
accompanied by a TRUMPETER with a MUTE, a DOUBLE BASS PLAYER 
AND A GUY ON A SNARE.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">HARRIET, HARRIET HARD-HEARTED 
HARBINGER OF HAGGIS 
BEAUTIFUL, BEMUSED BELLICOSE BUTCHER
UNTRUST-ING
UNKNOW-ING
UNLOV-ING
HE WANTS YOU BACK HE SCREAMS INTO 
THE NIGHT AIR LIKE A FIREMAN GOING 
TO A WINDOW THAT HAS NO FIRE EXCEPT 
THE PASSION OF HIS HEART 
I AM LONELY,
IT'S REALLY HARD
THIS POEM SUCKS</dd>
</dl>

<p class="action">A crowd has gathered in the street and spectators group on 
their balconies. They break out into APPLAUSE. Charlie proudly 
takes the applause and bows to Harriet. She throws him a 
flower. He's won her back.</p>

<h2 class="full-slugline">INT. BATHTUB - HARRIET'S APARTMENT - NIGHT</h2>

<p class="action">Romantic with candles surrounding the tub. Harriet and Charlie 
are bathing together. Wherever one of them moves, the water 
extinguishes a candle and Charlie lights it. This is keeping 
him pretty busy.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I've been there for almost a year. I 
only planned on stay with her for a 
few weeks, but she gets upset every 
time I say I'm moving.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You were close as kids?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I pretty much raised her. You know 
the scene. Depressed mother... 
withdrawn father.</dd>
<dd class="parenthetical">(she remembers)</dd>
<dd class="dialogue">My dad was a photographer too.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Really?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">He hated it. Trudging off to those 
weddings every Saturday night. Other 
people's celebrations he called it. 
He said sometimes they didn't even 
offer him a glass of soda. He had a 
small studio, and every year at 
Christmas he'd take a picture of me 
and Rose and put it in the window on 
a little card that said "Seasons 
Greetings." Awful pictures. It's 
like... I could see his pain in my 
face. Anyway, me and my sister worked 
with our "childhood issues" in 
different ways. She became a 
photographer and I became phobic 
about having my picture taken. It's 
quite a family.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Where are they now? Your parents?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Dead. Car accident.</dd>
</dl>

<p class="action">There is a RING at the door.</p>

<dl>
<dt class="character">ROSE (O.S.)</dt>
<dd class="dialogue">Harriet, its for you.</dd>
</dl>

<h2 class="full-slugline">INT. HARRIET'S LIVING ROOM</h2>

<p class="action">Charlie comes out of the bathroom in a robe.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Charlie, I want you to meet a friend 
of mine. Say hi to Ralph.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(shocked)</dd>
<dd class="dialogue">Ralph?</dd>
</dl>

<p class="action">A plain looking lady in her thirties, RALPH, is sitting by 
the window.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(delighted)</dd>
<dd class="dialogue">Oh, like Ralph, the lady carpenter 
in Green Acres!</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">This is Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I love you!</dd>
</dl>

<dl>
<dt class="character">RALPH</dt>
<dd class="dialogue">It's nice to meet you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(ecstatic)</dd>
<dd class="dialogue">Nice? It's more than nice. It's great 
to meet you. It's fantastic to meet 
you. I just, I can't tell you how 
glad I am. Ralph. Really. I am.</dd>
</dl>

<dl>
<dt class="character">RALPH</dt>
<dd class="dialogue">Well, thank you, I've heard a lot of 
nice things about you too, and...</dd>
</dl>

<p class="action">He rushes over to hug her.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Oh, Ralphie, I love you.</dd>
</dl>

<p class="action">Swept up in his enthusiasm his towel falls off. Harriet is 
shocked, but amused.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I'll leave you guys alone. Have a 
great time.</dd>
</dl>

<p class="action">Charlie realizes he is naked. His arms are still wrapped 
around Ralph.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm naked, aren't I?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Why, yes, you are.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I should really get dressed now.</dd>
</dl>

<p class="action">He hurriedly puts his towel back on, bolts to the bedroom 
door. Just before he enters, he pauses and turns to Ralph.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(to Ralph)</dd>
<dd class="dialogue">Call me.</dd>
</dl>

<p class="action">He leaves.</p>

<dl>
<dt class="character">RALPH</dt>
<dd class="parenthetical">(to Harriet; a little 
<p class="action">        confused)</dd></p>
<dd class="dialogue">Friendly guy.</dd>
</dl>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">A KITCHEN DOOR OPENS...</h5>

<p class="action">and Charlie's mother, MAY, shoulders her way through the 
door, carrying a HAPPY ANNIVERSARY CAKE with a big 45 written 
on it.</p>

<h3 class="right-transition">PULL BACK TO REVEAL WE ARE IN:</h3>

<h2 class="full-slugline">INT. CHARLIE'S PARENTS' APARTMENT - NIGHT</h2>

<p class="action">May and Stuart's 45 year anniversary party. UNCLE ANGUS is 
at the piano playing "Happy Anniversary" as Charlie's parents, 
all their friends and Harriet all sit around the piano 
SINGING:</p>

<dl>
<dt class="character">THE GROUP</dt>
<dd class="dialogue">Happy Anniversary to you...</dd>
<dd class="parenthetical">(Etc.)</dd>
</dl>

<p class="action">The song ends. May and Stuart blow out the candles.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="dialogue">Okay, everyone come and get a piece 
of cake and some milk.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hey Dad, I got an anniversary present 
for you...</dd>
</dl>

<p class="action">Stuart looks up, and Charlie gets him in a headlock and pins 
him to the ground.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">I'm proud of you, son. I'm proud of 
you.</dd>
<dd class="parenthetical">(Stuart addresses the 
<p class="action">        group)</dd></p>
<dd class="dialogue">I just wanna propose a toast. To my 
wife. Forty five years ago today May 
and I got married. Some of you were 
there, some of you weren't born yet, 
some of you are now dead, but... We 
both said, "I do" and we haven't 
agreed on a single thing since. But, 
I'm glad I married you May cause... 
It could have been worse and 
besides... I still love you.</dd>
</dl>

<p class="action">They kiss and everyone APPLAUDS. Uncle Angus breaks into, 
"Stand By Your Man." May and Stuart start to dance. Charlie 
looks at another young couple who are touched by this sincere 
display of love. He looks over at Harriet. Stuart and May 
feed each other cake. Charlie approaches Harriet.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Harriet, I wanna talk to you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Boy, you really made some impression 
with Ralph. She can't get over you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(stalling; nervous)</dd>
<dd class="dialogue">I'm just so happy for you to have 
friends like Ralph. What a great 
friend to have.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Is everything all right, Charlie? 
You're perspiring.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Harriet... marry me.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I want to have a wedding. With you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">No.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Please.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I don't know, Charlie. It's so good 
like it is. Why don't we just live 
together first?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Because, I love you and I want you 
to marry me and be with me for 45 
years. I want you to have my children, 
and I want to have your children. I 
know that sounds like a lot of 
children, and they might not all get 
along, but... I'm finally ready to 
trust you and to make a commitment. 
Marry me, Harriet, please. Be my 
wife.</dd>
</dl>

<p class="action">Harriet flinches slightly at the word "Wife", but Charlie is 
too wrapped up in the moment to notice. Stuart addresses the 
group.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">I'd like to thank Charlie for throwing 
us this party. I hope some day you 
have the same great 45 years that 
we've had.</dd>
</dl>

<p class="action">People clap and smile. Harriet looks at Charlie. He has tears 
in his eyes.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yes.</dd>
</dl>

<p class="action">At first it doesn't register. Then...</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You will?</dd>
</dl>

<p class="action">She smiles.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Let's get married, Charlie.</dd>
</dl>

<p class="action">They kiss.</p>

<dl>
<dt class="character">MAY</dt>
<dd class="parenthetical">(from across room)</dd>
<dd class="dialogue">Harriet, come here a minute. I want 
you and Uncle Angus to play a song 
together.</dd>
</dl>

<p class="action">Harriet and Charlie kiss one last time and she goes to the 
piano.</p>

<p class="action">Charlie stays in the corner, and Tony comes over.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Hey, sorry I'm so late. What's 
happening?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nothing. Nothing at all. Just two 
little things...</dd>
<dd class="parenthetical">(as Tony looks in)</dd>
<dd class="dialogue">That woman over there in the corner... 
She's Harriet's friend, and her name 
is Ralph.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">No shit.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">And secondly... That woman over 
there...</dd>
<dd class="parenthetical">(Re: Harriet)</dd>
<dd class="dialogue">That's Harriet, and we're getting 
married.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(excited)</dd>
<dd class="dialogue">Fantastic... What did I tell you. 
She's a great girl. And the last 
thing in the world she'd be is a 
murderer.</dd>
</dl>

<p class="action">And then Harriet begins singing at the piano.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(singing)</dd>
<dd class="dialogue">ONLY YOU...
CAN MAKE THIS WORLD SEEM RIGHT...
ONLY YOU...
CAN MAKE THIS DARKNESS LIGHT..."</dd>
</dl>

<p class="action">Tony and Charlie look at each other. "Only you?" Then Charlie 
looks at his bride with confidence.</p>

<p class="action">He walks over and joins her. She sings to him. It's a moment.</p>

<h2 class="full-slugline">INT. JEWELRY STORE - DAY</h2>

<p class="action">Charlie and Harriet pick out a diamond ring.</p>

<h2 class="full-slugline">INT. TRAVEL AGENCY</h2>

<p class="action">Charlie and Harriet point to brochures of the different cities 
they could go to on their honeymoon. They decide on a picture 
of the "DRY CREEK LODGE" in Oregon.</p>

<h2 class="full-slugline">INT. DOCTOR'S OFFICE</h2>

<p class="action">They are getting their blood tests back. Harriet looks at 
hers, casually. Charlie is nervous. Reluctantly he opens the 
file and looks at it. He is pleased with the results and 
does a victory dance.</p>

<h2 class="full-slugline">EXT. SCOTTISH PRESBYTERIAN CHURCH - ESTABLISHING</h2>

<h5 class="goldman-slugline">INT. SCOTTISH PRESBYTERIAN CHURCH</h5>

<p class="action">Charlie and Harriet are being married. Harriet is in a 
beautiful wedding gown. Charlie is wearing a kilt. Tony is 
the best man. He also wears a kilt. Stuart, also kilted, 
May, the whole family along with a hundred well-wishers are 
in attendance. The SCOTTISH MINISTER presides. Rose is in a 
kilt.</p>

<dl>
<dt class="character">SCOTTISH MINISTER</dt>
<dd class="dialogue">Now, Mr. MacKenzie, if you will take 
this woman to be your wife, through 
thick and thin, for better or for 
worse, please say: "I do"...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I do...</dd>
</dl>

<dl>
<dt class="character">SCOTTISH MINISTER</dt>
<dd class="dialogue">Now Harriet, if you will take this 
man, through good times and bad, for 
ever and ever, as your husband, please 
say "I Do"...</dd>
</dl>

<p class="action">Harriet starts to speak; but right before the words come 
out, she stares into Charlie's eyes and STOPS. Charlie looks 
nervous. So does the Scottish Minister. So does Tony. So 
does everyone.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(after a long pause; 
<p class="action">        finally:)</dd></p>
<dd class="dialogue">I do.</dd>
</dl>

<dl>
<dt class="character">SCOTTISH MINISTER</dt>
<dd class="dialogue">Now Charlie... Kiss the beautiful 
bride!</dd>
</dl>

<p class="action">Charlie and Harriet kiss. We can see (though Charlie can't) 
Harriet has a strange unsure expression on her face. Tony 
notices it though and can't figure it out.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">Let's get pissed.</dd>
</dl>

<p class="action">The wedding march kicks in being played by a drunken Scotsman 
on BAGPIPES.</p>

<h2 class="full-slugline">INT. RECEPTION HALL</h2>

<p class="action">A Scottish accordionist and a Drummer play SCOTLAND THE BRAVE. 
Some OLDER SCOTTISH AUNTIES are CLAPPING and HOOTING LOUDLY 
along with the tune. Some young girl COUSINS in traditional 
Scottish costume, dance the sword dance along to SCOTLAND 
THE BRAVE.</p>

<p class="action">We pass the buffet which we see is catered by "Meats Of The 
World." Then we pass a very drunken Stuart in a heated 
discussion with four other people.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="dialogue">You know Golden Gate park was designed 
by a Scotsman, MacClaren, which is 
who MacClaren park was named after.</dd>
</dl>

<p class="action">The others agree heartily.</p>

<p class="action">May and Tony are dancing. May is dancing uncomfortably close. 
She keeps sliding her hand down to his ass, which he then 
has to move back to his shoulder.</p>

<p class="action">Then we come to William, who's reluctantly at the children's 
table. All his little cousins are queuing up for a chance to 
feel his head.</p>

<p class="action">We find Charlie in a corner. One of the hooting Scottish 
aunties is trying to get him to have another Scotch.</p>

<dl>
<dt class="character">AUNTIE MOLLY</dt>
<dd class="parenthetical">(proffering the Scotch)</dd>
<dd class="dialogue">Charlie, get this down your neck.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Auntie Molly if I have another one 
I'll end up underneath the table 
with my kilt over my head.</dd>
</dl>

<p class="action">Tony joins them.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Where's Harriet?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I don't know. Oh, there she is.</dd>
</dl>

<p class="action">She's in the corner by herself looking weird and ominous. 
She has enough food in front or her for three people.</p>

<p class="action">She eats ravenously and incessantly. Charlie goes over to 
her.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">A little hungry, were you?</dd>
</dl>

<p class="action">At that moment, a FLASH goes off. Harriet looks up angrily.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What are you...!</dd>
</dl>

<p class="action">Then she realizes it's Rose. She calms down and smiles. 
Charlie looks at her, a little peculiarly, but Harriet regains 
her composure.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Sorry. The flash just...</dd>
</dl>

<p class="action">The band kicks into a new dance. A YOUNG BOY comes up to the 
bagpipe man with a shot of whiskey and whispers into his 
ear. The bagpipe man stops the song, downs the whiskey and 
then breaks into Rod Stewart's "IF YOU THINK I'M SEXY." From 
across the room we hear Stuart singing.</p>

<dl>
<dt class="character">STUART</dt>
<dd class="parenthetical">(full volume; singing)</dd>
<dd class="dialogue">IF YOU THINK I'M SEXY...
AND YOU WANT MY BODY...
COME ON BABY LET ME KNOW.</dd>
</dl>

<p class="action">Stuart gives the Bagpiper the thumbs up. The young people in 
the room start to jam, and then one by one the other guests 
start getting into the swing of things. The bagpipe man 
continues playing. It is clear that he is far too drunk to 
play. He slowly keels over, drunk. And as he falls over face 
first, he lands on his Bagpipes. The bagpipes let out an 
ATONAL DEFLATING SOUND like the last dying throes of a 
tortured animal. The BAGPIPE WAIL extends into the next scene.</p>

<h2 class="full-slugline">EXT. HIGH ABOVE COAST - DAY</h2>

<dl>
<dt class="character">CHARLIE (V.O.)</dt>
<dd class="dialogue">Wait 'til you see this place, Harriet.</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE'S CAR - DAY</h2>

<p class="action">They drive along the beautiful coast. Harriet is still eating. 
They're listening to TEENAGE FAN CLUB.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">This is Teenage Fan Club. They're 
from Scotland.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">They're great.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">We'll have the whole lodge to 
ourselves practically.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I can't wait, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I wish you could be me, so you could 
know how great it feels to be with 
you.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It sounds wonderful</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Do you think that would be a good 
line for a poem?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Honestly? It sounds a little Hallmark.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yeah, it's a little Seals and Croft. 
I have a habit of sabotaging 
relationships, and there were a 
million times during me and you that 
I could have blown this, and I just 
thank God that I didn't...</dd>
</dl>

<h2 class="full-slugline">INT. POLICE STATION - DAY</h2>

<p class="action">Tony is at his desk. The captain kicks open the door, knocking 
Tony's feet off the desk. The captain is now dressed in 
suspenders, a loosened tie, and a shirt with pit stains.</p>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">O.K., Spiletti, I got word from 
upstairs that you been pokin' your 
nose into that Ralph Elliot case.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Yes, Captain.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Don't "yes, Captain" me, Spiletti. 
You're outta line. This is strictly 
homicide.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Captain, I got this friend...</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Friend? Yeah, we all got friends, 
Spiletti. I'm warning you, Stay away 
from this one. Back off, Italian 
boy. You're getting too close to 
this one.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Captain, I know what I'm doing. Trust 
me. What's the news.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">I can't believe I'm doing this, but 
that girl who confused to Ralph 
Elliot's murder also confessed to 
other murders.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I knew she would! I knew it!</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Yeah, apparently she also confessed 
to killing Abe Lincoln, Julius Caesar, 
and Warren G. Harding. She's a nut, 
Spiletti!</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(getting up)</dd>
<dd class="dialogue">Oh, my god! I gotta go!</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="dialogue">Yeah, screw this one up Spiletti and 
you'll be writing parking tickets 
for the rest of your days.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I won't let you down, Captain.</dd>
</dl>

<p class="action">Tony exits for a beat, then pokes his head in the doorway.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">That's much better Captain.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="parenthetical">(nice again)</dd>
<dd class="dialogue">You think so? Well, thank you very 
much.</dd>
</dl>

<h2 class="full-slugline">EXT. POLICE STATION - DAY</h2>

<p class="action">Tony hurries to his car.</p>

<h2 class="full-slugline">EXT. GAS STATION ALONG THE COAST - DAY</h2>

<p class="action">They stop at a gas station with a small mini-mart. As Charlie 
is filling the tank he notices Harriet slipping the key out 
of the ignition before she walks to the mini-mart for more 
food.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You want anything?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Lamb chops, creamed spinach, stuffed 
tomatoes and a Hershey Bar.</dd>
</dl>

<p class="action">Harriet arrives at the little Ma and Pa type mini-mart and 
smiles to Charlie.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">If they don't have all that, I'll 
just take the Hershey bar.</dd>
</dl>

<h2 class="full-slugline">EXT. CHARLIE'S APARTMENT BUILDING - DAY</h2>

<p class="action">Tony stands at the door, buzzing the buzzer to no response.</p>

<h2 class="full-slugline">INT. CHARLIE'S CAR - EARLY EVENING</h2>

<p class="action">They are still driving along the coast. Charlie is eating 
his Hershey Bar. Harriet's eyes are becoming a bit glazed 
now, her movements a little static. She keeps looking behind 
them and out the window.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What do you keep looking behind us 
for?</dd>
<dd class="parenthetical">(joking)</dd>
<dd class="dialogue">Is someone following you, or...?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">They were. I think they're gone.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(curious; pausing)</dd>
<dd class="dialogue">What do you mean, they were?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">The gas station guy. I thought he 
was chasing us for a while, but I 
guess he stopped.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">The gas station guy? Why would the 
gas station guy chase us.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I don't know, Charlie. I guess for 
not paying.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What do you mean not paying? You 
didn't pay him for the gas.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I forgot to pay... I didn't want to 
be away from you for any longer.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">So, you just left.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Yes. And you're an accomplice.</dd>
</dl>

<p class="action">He stops mid-bite on his Hershey Bar. He's confused.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm not sure I understand.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Look, Charlie, don't you get it? 
We're a team.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(going with it)</dd>
<dd class="dialogue">I can play that game. I'll get the 
next gas station. Like Bonnie &#38; Clyde.</dd>
</dl>

<p class="action">He and Bonnie continue on the winding road and pass a sign 
that reads: "DRY CREEK LODGE - 40 MILES"</p>

<h2 class="full-slugline">INT. HALLWAY - HARRIET'S APARTMENT - DAY</h2>

<p class="action">Tony knocks. No answer. He picks the lock and enters.</p>

<h2 class="full-slugline">INT. LIVING ROOM - DAY</h2>

<p class="action">Rose is tied up and lying in a pool of blood. Tony stops for 
a beat, draws his gun and slowly walks over to her. Just as 
he gets there, a SHUTTER CLICKS.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Oh, hi!</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(practically hysterical)</dd>
<dd class="dialogue">What is it with the women in your 
family?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">I was just doing a murder series in 
honor of the wedding.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Hey, this is real blood.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Yes, Harriet, give it to me. She's a 
butcher.</dd>
<dd class="parenthetical">(Tony reacts)</dd>
<dd class="dialogue">...She owns a butcher shop.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I need a picture of Harriet.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Sorry. No can do.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You took a picture at the party. I 
saw it.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">It didn't come out.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Look, Rose. I need a photo.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">The picture didn't come out.</dd>
<dd class="parenthetical">(he waits)</dd>
<dd class="dialogue">It was unflattering. In made her 
look ten pounds heavier.</dd>
<dd class="parenthetical">(he waits)</dd>
<dd class="dialogue">She's my sister.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">She's been implicated in a crime. I 
need the photo to eliminate her as a 
suspect.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">And if she's not innocent. If she's, 
you know, "quirky?"</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">If she's "quirky" we'll save Charlie's 
life.</dd>
</dl>

<p class="action">Rose pulls out a photograph &#8209;&#8209; Charlie and Harriet. Looking 
young and in love.</p>

<h2 class="full-slugline">EXT. DRY CREEK LODGE - LATE IN THE DAY</h2>

<p class="action">A beautiful old Colonial Mansion, nestled in the mountains 
and forests of the North-West. Romantic and from another 
day. Charlie and Harriet pull up in front of it.</p>

<p class="action">The Valets open the door for them.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's like a castle, Charlie. It's so 
beautiful.</dd>
</dl>

<dl>
<dt class="character">VALET</dt>
<dd class="dialogue">Welcome to the Dry Creek. You just 
beat the rainstorm. Two hours later 
and the roads'd probably be closed.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Great. If you could help us with the 
luggage, we have these two in the 
back seat and...</dd>
</dl>

<p class="action">As they deal with the luggage, Harriet starts to walk away 
from the hotel, away from the car, rain falling on her head. 
She walks straight at the CAMERA, so only we can see her 
expression. Her expression is one of simply "losing it".</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Harriet? What are you doing honey?</dd>
</dl>

<p class="action">Harriet turns around and smiles at Charlie. He smiles back.</p>

<h2 class="full-slugline">INT. LOBBY OF DRY CREEK LODGE - EVENING</h2>

<p class="action">Charlie and Harriet stand at the desk. Harriet is not quite 
paying attention. Her attention span has slipped to none. 
She's fidgety. She looks around suspiciously at everything 
and everyone.</p>

<dl>
<dt class="character">DESK CLERK</dt>
<dd class="dialogue">Welcome, Sir. We have you with us 
for four nights, Mr. MacKenzie. Dinner 
reservations are at eight-thirty.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Great. Sounds terrific.</dd>
</dl>

<dl>
<dt class="character">DESK CLERK</dt>
<dd class="dialogue">Also, you might wanna prepare some 
candles by the bed. We're expecting 
the rainstorm to get even worse. We 
might even lose the power tonight.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Did you hear that, Harriet? A storm. 
I can't think of anything more 
romantic than the two of us trapped 
in our room in the middle of a rain 
storm.</dd>
<dd class="parenthetical">(noticing her)</dd>
<dd class="dialogue">You okay, Harriet?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Just a little head-ache.</dd>
<dd class="parenthetical">(to clerk)</dd>
<dd class="dialogue">Excuse me, is there a drug store in 
the hotel? I want to get some aspirin.</dd>
</dl>

<dl>
<dt class="character">DESK CLERK</dt>
<dd class="dialogue">Right beyond those trees, Ma'm. 
Anything you need.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Thanks. Don't go anywhere. I'll be 
right back.</dd>
</dl>

<p class="action">Harriet walks off to the lobby store, backwards, looking at 
Charlie. Charlie watches her walk off. The Desk Clerk sits 
staring at Charlie.</p>

<dl>
<dt class="character">DESK CLERK</dt>
<dd class="dialogue">You think she's really got a head-
ache?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What?</dd>
</dl>

<dl>
<dt class="character">DESK CLERK</dt>
<dd class="dialogue">Ah, nothing. Here's your key. You're 
in the Oak Room.</dd>
</dl>

<p class="action">Charlie looks back at the drug store, where Harriet is 
shopping. She waves to him. Charlie looks back at the Desk 
Clerk and grabs the key.</p>

<h3 class="right-transition">CUT TO:</h3>

<h5 class="goldman-slugline">FAX OF THE PHOTO OF CHARLIE &#38; HARRIET</h5>

<p class="action">coming out of a fax machine.</p>

<h2 class="full-slugline">INT. WALTER'S PLUMBING - EARLY EVENING</h2>

<p class="action">WALTER, the owner of the Plumbing store, dressed in overalls 
takes the Fax out and then picks up the phone.</p>

<dl>
<dt class="character">WALTER</dt>
<dd class="dialogue">That's Ralph Elliot's wife, alright. 
She had shorter hair in those days.</dd>
</dl>

<h2 class="full-slugline">INT. MARTIAL ARTS STUDIO - NIGHT</h2>

<p class="action">MASTER CHO, the new owner of the studio, dressed in a gee, 
looks at the same fax.</p>

<dl>
<dt class="character">MASTER CHO</dt>
<dd class="parenthetical">(into phone)</dd>
<dd class="dialogue">Mrs. Richter gain much weight since 
then, but it's definitely her.</dd>
</dl>

<h2 class="full-slugline">INT. THE LIZARD'S LOUNGE - ATLANTIC CITY</h2>

<p class="action">RANDY ROMANO, the owner, talks into the phone, holding up 
the faxed photo of Charlie and Harriet.</p>

<dl>
<dt class="character">RANDY</dt>
<dd class="dialogue">That's his little lollipop, alright. 
Boy he loved her. I'll tell you, she 
was a lot of fun. Smart. A doll face 
to boot.</dd>
</dl>

<h2 class="full-slugline">INT. TONY'S CUBICLE - NIGHT</h2>

<p class="action">Tony is on the phone. Kathy, seen before at the police 
station, stands with him.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Circuits are out from the storm.</dd>
</dl>

<p class="action">Tony gets to the police station door and opens it. Kathy 
follows him.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(to Kathy)</dd>
<dd class="dialogue">Keep trying the hotel. Tell the chief 
I just chartered a plane up to Oregon.</dd>
</dl>

<p class="action">The Police Captain enters.</p>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="parenthetical">(points to his hair)</dd>
<dd class="dialogue">See that Spiletti &#8209;&#8209; A gray hair! 
Every day, Spiletti, I find another 
one. And that's all due to you. Get 
out there, and catch me some bad 
guys!</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Not now, Captain.</dd>
</dl>

<dl>
<dt class="character">CAPTAIN</dt>
<dd class="parenthetical">(nice again)</dd>
<dd class="dialogue">Sorry.</dd>
</dl>

<p class="action">Tony dashes out of the police station and into his car.</p>

<h2 class="full-slugline">INT. CHARLIE'S HOTEL ROOM - NIGHT</h2>

<p class="action">A beautiful suite, with a fireplace burning a big stack of 
wood, with another stack next to it, with an AXE in it. Music 
is playing softly on the stereo. And Charlie and Harriet 
have just finished making love underneath the covers, 
illuminated just by the light of the fireplace.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">This is the best honeymoon I could 
ever imagine, Harriet. If we had to 
pack and go home right now, I'd still 
think it was the greatest honeymoon 
ever.</dd>
</dl>

<p class="action">Harriet doesn't respond. Her head is turned from his.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Don't you agree, Harriet? Harriet?</dd>
</dl>

<p class="action">He pulls the sheets away from her face to see that she is 
crying.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(wiping her tears)</dd>
<dd class="dialogue">What? What are you crying? What is 
it?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's nothing. It's just... I was 
just thinking... We're married now. 
And I always wanted to try and have 
kids, and...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">So do I. Look, there's nothing more 
I'd like to do than have, kids, or...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's just, I get scared that certain 
things will happen, or...</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What are you talking about? You're 
gonna be a great Mom. I know you 
will.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It's not that, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What then?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You're gonna laugh.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Tell me. Of course I'm not gonna 
laugh. Kids is a big thing. It's 
hard. I'm sure I have the same fears.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">If we have kids, Charlie, things 
happen. Kids are healthy and fine, 
and some aren't, and I don't know if 
I could live with myself if I gave 
birth to a child with webbed feet.</dd>
</dl>

<p class="action">Charlie stops to think about this. Webbed feet?</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Webbed feet?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You're laughing.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, I'm not laughing.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">You think that's silly?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">No, no. That's a natural fear. I've 
thought about that fear.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">It really worries me, Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(quite confused)</dd>
<dd class="dialogue">Well, look, they have, doctors &#8209;&#8209; I 
assume &#8209;&#8209; that deal, only with, webbed 
feet. And, God Forbid, and I'm talking 
strictly hypothetically, should that 
happen, we'll find one.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(kissing him; happy 
<p class="action">        now)</dd></p>
<dd class="dialogue">You're the greatest Charlie.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(confused, to say the 
<p class="action">        least)</dd></p>
<dd class="dialogue">Thanks. We should get ready for 
dinner.</dd>
</dl>

<h2 class="full-slugline">EXT./INT. CHARTERED CESSNA - NIGHT</h2>

<p class="action">A small plane flies through the clouds. It's just Tony and 
DENNIS the pilot.</p>

<p class="action">Dennis never really realizes this is more than a sightseeing 
tour, and constantly points out scenic points along the way.</p>

<dl>
<dt class="character">DENNIS</dt>
<dd class="dialogue">Out your left side, you can see the 
Sierra Nevada, which is the largest 
mountain range west of the Rockies...</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Great. Rockies. I don't care. Oregon. 
Move.</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE'S HOTEL ROOM AT DRY CREEK LODGE - NIGHT</h2>

<p class="action">Charlie is dressed very sharply in sport coat and tie. He 
yells into the bathroom where we can see part of Harriet 
from behind.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">You almost ready? The first seating 
is in five minutes.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(from other room)</dd>
<dd class="dialogue">I just wanna look good for you, 
Charlie. That's all.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I'm sure you look great. I'm sure 
you look...</dd>
</dl>

<p class="action">Harriet turns the corner, wearing a nice dress. Her hair 
looks okay. She's wearing perfume. The only problem is, she 
has two lines of mascara running down her cheeks. She's been 
crying. Charlie looks curious.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Do I look okay, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes. Well...</dd>
</dl>

<p class="action">Charlie points to his own eye.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What's wrong?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nothing. You kind of look like Tammy 
Faye Baker right now.</dd>
</dl>

<p class="action">She looks in the mirror.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Oh, yeah.</dd>
</dl>

<p class="action">She goes back into the bathroom.</p>

<h2 class="full-slugline">EXT. CESSNA - NIGHT</h2>

<p class="action">The plane descends towards the runway. The rain comes down 
hard.</p>

<dl>
<dt class="character">DENNIS (V.O.)</dt>
<dd class="dialogue">As we prepare to land, we can see 
off to our left Lake Shanony, which 
is...</dd>
</dl>

<dl>
<dt class="character">TONY (V.O.)</dt>
<dd class="dialogue">Just land. Don't worry about Lake 
Shanony. I don't give a shit about 
Lake Shanony.</dd>
</dl>

<p class="action">The plane touches down.</p>

<h2 class="full-slugline">EXT. DRY CREEK LODGE - NIGHT</h2>

<p class="action">Rain pours fantastically on the gothic castle. Wind blows 
hard.</p>

<dl>
<dt class="character">MAN'S VOICE (V.O.)</dt>
<dd class="dialogue">A toast to our new friends, Charlie 
and Harriet...</dd>
</dl>

<h2 class="full-slugline">INT. BEAUTIFUL FRENCH RESTAURANT IN HOTEL - NIGHT</h2>

<p class="action">A beautiful dining room with a small dance floor. Charlie 
sits at an intimate table for two with Harriet. A small band 
plays in the b.g., as the BAND LEADER is making the toast. 
The five or six other couples in the restaurant also hold up 
their glass.</p>

<dl>
<dt class="character">BAND LEADER</dt>
<dd class="dialogue">...we're honored to be here for this 
very special day in...</dd>
</dl>

<p class="action">The CONCIERGE at this point interrupts to bring Charlie a 
TELEPHONE. Everyone stops and watches and waits.</p>

<dl>
<dt class="character">CONCIERGE</dt>
<dd class="dialogue">I'm sorry to interrupt, Sir. There's 
a phone call for you from town.</dd>
<dd class="parenthetical">(Charlie takes phone)</dd>
<dd class="dialogue">They say it's quite urgent.</dd>
</dl>

<p class="action">The toast, as well as the entire room, stops &#8209;&#8209; almost like 
an E.F. Hutton commercial, waiting for Charlie's phone call 
to finish.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(curious)</dd>
<dd class="dialogue">Hello?</dd>
</dl>

<h2 class="full-slugline">INT. AIRPORT IN OREGON - EARLY EVENING</h2>

<p class="action">Tony speaks into the phone frantically.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Charlie, you okay?</dd>
</dl>

<h5 class="goldman-slugline">INTERCUT PHONE CALL</h5>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Great. Couldn't be better.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Charlie, listen to me! It's her! 
Harriet is Mrs. X! She killed Ralph 
and the two other men!</dd>
</dl>

<p class="action">Charlie looks up across the table at Harriet who is completely 
caught up in the event of seeing how long she can keep her 
hand in the candle before it hurts.</p>

<p class="action">She puts it in, smiles then takes it out. She shakes her 
hand, and repeats the process.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(talking softly)</dd>
<dd class="dialogue">Look, that's great &#8209;&#8209; it just so 
happens though, that I met...</dd>
<dd class="parenthetical">(louder than he had 
<p class="action">        hoped)</dd></p>
<dd class="dialogue">Ralph, and much to my delight, not 
only is she alive, but she's female. 
I thought I told you.</dd>
</dl>

<p class="action">Harriet looks at Charlie, very suspiciously. He looks back 
at her, and tries to smile, pretending that he is having a 
pleasant, and completely irrelevant conversation.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Rose had a picture. It checked out. 
It's her, Charlie. She is the 
murderer.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Charlie, your food is getting cold.</dd>
</dl>

<p class="action">Charlie waves "One Minute" to Harriet, as she watches.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">So, what do I do?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I called the police. All the roads 
are closed, but they're on their 
way. In the mean time just...</dd>
</dl>

<p class="action">The line goes DEAD.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Hello?</dd>
<dd class="parenthetical">(pressing receiver)</dd>
<dd class="dialogue">Hello?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What's a matter, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(to concierge)</dd>
<dd class="dialogue">The phone just went dead. I was on 
the phone and it went dead.</dd>
</dl>

<dl>
<dt class="character">CONCIERGE</dt>
<dd class="dialogue">That's quite common, sir. I'm sure 
the lines'll be out in the whole 
city 'til tomorrow. Enjoy your meal, 
Sir.</dd>
</dl>

<p class="action">The Concierge takes the phone away. Charlie turns slowly to 
Harriet, genuinely scared.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">What happened, Charlie?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nothing... Nothing happened. Just 
the lines are down. Phone lines.</dd>
</dl>

<p class="action">Suddenly, the band leader continues with his toast.</p>

<dl>
<dt class="character">BAND LEADER</dt>
<dd class="parenthetical">(over microphone)</dd>
<dd class="dialogue">...so to these two young people, we 
wish them a long and happy life 
together and would like to play their 
song. The Platters &#8209;&#8209; "Only You".</dd>
</dl>

<p class="action">The band starts to play "Only You".</p>

<p class="action">People APPLAUD. Harriet and Charlie just stare at each other. 
He knows.</p>

<p class="action">The older couple at the next table, MR. &#38; MRS. LEVENSTEIN, 
lean over to their table.</p>

<dl>
<dt class="character">MR. LEVENSTEIN</dt>
<dd class="dialogue">How about the traditional Bride &#38; 
Groom dance?</dd>
</dl>

<p class="action">Another couple walks by and pulls them literally out of the 
their seats and onto the dance floor.</p>

<dl>
<dt class="character">OTHER COUPLE</dt>
<dd class="dialogue">Come on. It's a tradition.</dd>
</dl>

<p class="action">Charlie finds himself in the middle of the dance floor dancing 
slowly with Harriet. He's scared out of his mind. The music 
plays in the background. Harriet smiles strangely at him. He 
tries to smile back, checking all the Exits, planning an 
escape.</p>

<p class="action">Then suddenly, call it luckily, MR. LEVENSTEIN, interrupts:</p>

<dl>
<dt class="character">MR. LEVENSTEIN</dt>
<dd class="dialogue">Excuse me. Could I cut in on your 
dance?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Of course. Sure...</dd>
</dl>

<p class="action">Charlie gives her hand away to Mr. Levenstein. He takes Mrs. 
Levenstein's hand and starts to dance towards the EXIT, when 
suddenly the ELECTRICITY GOES OUT. The MUSIC is out. The 
LIGHTS ARE OUT.</p>

<p class="action">In the dimmest of lights provided from the cloud covered 
moon outside, Charlie runs across the dance floor, fighting 
for an exit to the outside.</p>

<p class="action">He arrives in someone's arms on his way.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I need your help! You have to help 
me! I've married a...!</dd>
</dl>

<p class="action">The LIGHTS GO BACK ON and Charlie is in HARRIET'S ARMS again. 
Her face is near menacing now. She smiles a very disturbed 
grin. He doesn't know what to say.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(much too pleasant)</dd>
<dd class="dialogue">Hello, Charlie.</dd>
</dl>

<p class="action">Charlie and her are squared off. Neither speak. Suddenly 
both of them are lifted into the air. They look down and see 
the waiters and busboys picking them up onto chairs, throwing 
them up in the air again and again. The MUSIC plays along 
loudly.</p>

<p class="action">Harriet watches Charlie very closely, as Charlie looks scared. 
Then, the people start to carry them out of the room and 
down the hallway.</p>

<dl>
<dt class="character">WAITER</dt>
<dd class="dialogue">Let's take 'em to their room.</dd>
</dl>

<dl>
<dt class="character">CONCIERGE</dt>
<dd class="dialogue">Yeah, I'm sure they've had enough of 
these crowds for one night.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">My dinner. I didn't finish my dinner 
yet.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Smile, Charlie. Act like you're having 
a good time.</dd>
</dl>

<h2 class="full-slugline">INT. OREGON AIRPORT - SAME/NIGHT</h2>

<p class="action">Tony is talking to an attractive young girl behind the airport 
Rent-A-Car booth.</p>

<dl>
<dt class="character">RENT-A-CAR GIRL</dt>
<dd class="dialogue">I'm sorry, Sir. The roads are all 
closed. We can't rent any cars this 
evening.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You have to rent me something. I've 
gotta get up there. My friend's in 
danger...</dd>
</dl>

<h2 class="full-slugline">INT. CHARLIE &#38; HARRIET'S ROOM - NIGHT</h2>

<p class="action">The other hotel guests threw them inside. The room is all 
made up, the sheets are pulled down, the firewood is cut, 
the AXE is in the wood.</p>

<dl>
<dt class="character">CONCIERGE</dt>
<dd class="dialogue">Have a good night, you two.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Come on in. Stay for a nightcap.</dd>
</dl>

<dl>
<dt class="character">BELLBOY</dt>
<dd class="dialogue">No, you two wanna be alone. See you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(demanding)</dd>
<dd class="dialogue">Stay for a nightcap!</dd>
</dl>

<dl>
<dt class="character">BELLBOY</dt>
<dd class="dialogue">Sir, I really don't think I should</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(shouting)</dd>
<dd class="dialogue">STAY FOR A NIGHTCAP!</dd>
</dl>

<p class="action">The bellboy is frightened and runs away.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(shouting down the 
<p class="action">        hall after him)</dd></p>
<dd class="dialogue">STAY FOR A NIGHTCAP!</dd>
</dl>

<p class="action">Harriet pulls Charlie back into the room, frightened that 
he's leaving.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Don't go, Charlie.</dd>
</dl>

<h2 class="full-slugline">EXT. AIRPORT - NIGHT</h2>

<p class="action">Tony runs out of the airport terminal where he sees a man in 
his forties who's just entered his four wheel drive jeep.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(flashing his badge)</dd>
<dd class="dialogue">Excuse me, Sir, I'm with the San 
Francisco Police Department. I'm on 
official business and I'm afraid I 
have to commandeer your vehicle.</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="parenthetical">(unfazed)</dd>
<dd class="dialogue">No.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">What do you mean no?!</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">I happen to know for a fact that you 
don't have the power to commandeer 
my vehicle.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">This is true.</dd>
<dd class="parenthetical">(pause)</dd>
<dd class="dialogue">Please can I commandeer your vehicle?</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Well, where are you going?</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">To The Dry Creek Lodge.</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">I'll give you a lift.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Well, I don't want a lift, I really 
want to commandeer the vehicle. Please 
just let me commandeer the vehicle.</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Why don't you just let me drive you 
there? Really, I don't mind, it's on 
my way.</dd>
<dd class="parenthetical">(pause)</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">You're not going to bend on the 
commandeering thing are you?</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">No.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Well, if we get stopped will you at 
least let me say that I commandeered 
the vehicle, but I let you drive?</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">I'm uncomfortable with that.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Please?</dd>
</dl>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">All right.</dd>
</dl>

<h2 class="full-slugline">INT. CHARLES AND HARRIET'S ROOM</h2>

<p class="action">Charlie &#38; Harriet are all alone. The voices trail off down 
the hallway until they disappear. Charlie and Harriet stare 
at each other. Harriet blocks the door. Charlie looks around 
the room. The Axe. The Corkscrew. The letter opener. The 
fountain pen. At this point, everything in the room looks 
like a potential weapon. Harriet takes the axe.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I heard you on the phone before, 
Charlie. There's something I've got 
to tell you.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(frightened)</dd>
<dd class="dialogue">Harriet, I...</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I've been married before.</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">I already know.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">About my husbands?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Yes. And I was meaning to have a 
word with you. We could get an 
annulment.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(screams)</dd>
<dd class="dialogue">AAAhhhhhhh!</dd>
</dl>

<p class="action">Suddenly the power goes off again. They're both in the dark. 
A scuffle. Charlie has restrained Harriet, throws her in a 
walk-in closet and locks it. From behind the door, we hear 
Harriet WAILING. Which continues.</p>

<p class="action">Charlie picks up the axe, looks at it, relieved at his lucky 
escape. He rushes to the door to escape. He opens it and 
standing there is Rose.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Aaaaah, Rose, I never thought I'd be 
so glad to see you.</dd>
</dl>

<p class="action">Rose smiles. Charlie puts down the axe. The lights flicker 
back on.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="parenthetical">(going to the phone)</dd>
<dd class="dialogue">Maybe the phones are working again 
by now.</dd>
</dl>

<p class="action">He listens for a dial tone. Beside the phone he sees a note. 
He starts to read is:</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">'Dear, Harriet. I just can't handle 
the commitment. I'm leaving you.' 
Signed, 'Charlie.'</dd>
</dl>

<p class="action">And behind him Rose approaches with the axe raised.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What the hell is this? I didn't write 
this?</dd>
</dl>

<p class="action">And at that moment he turns to find the AXE BEING FLUNG 
THROUGH THE AIR AT his head. He ducks just in time.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What the fuck?!</dd>
</dl>

<p class="action">She takes another swing and she hits the lamp off the desk 
and the room is in complete DARKNESS.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Charlie. Why did you marry Harriet? 
I warned you not to marry her, didn't 
I? I warned all of them. But none of 
them listened to me. They all went 
ahead and married her. She's the 
pretty one. Where's Harriet? What 
have you done with my sister, Harriet?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Nothing, Rose.</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">If you've done something to my sister, 
Harriet, I swear to God I'll kill 
you.</dd>
</dl>

<p class="action">We stay in Charlie's hip pocket as he tries to get away from 
what he can't see. He stays very silent.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(from the closet)</dd>
<dd class="dialogue">Where are you, Charlie? What's going 
on?</dd>
</dl>

<p class="action">Then Rose strikes a match. She lights a candle and comes 
toward him. He looks around. The window is open. And Charlie 
is gone.</p>

<h2 class="full-slugline">INT. COMMANDEERED CAR - NIGHT</h2>

<p class="action">Tony and the commandeered man drive through the swampy, 
winding road on the way up to the hotel. Tony is drumming on 
the dash.</p>

<dl>
<dt class="character">MAN</dt>
<dd class="dialogue">Could you stop doing that please?</dd>
</dl>

<h2 class="full-slugline">EXT. CASTLE-LIKE ROUND TOWER/LEDGE OF TOWER - NIGHT</h2>

<p class="action">Charlie tightropes along the ledge of the building. The storm 
continues. Rose comes out on the ledge and starts to chase 
him. He rounds the bend. Charlie looks into one room and 
sees MR. &#38; MRS. LEVENSTEIN there. There's loud OPERA MUSIC 
playing in the room</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Call the police!</dd>
</dl>

<h2 class="full-slugline">INT. THE LEVENSTEIN'S ROOM - NIGHT SAME</h2>

<p class="action">The Levensteins prepare for bed. Charlie races by their 
window. Then Rose races by.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Call the police!</dd>
</dl>

<p class="action">Mr. Levenstein closes the curtains. He can't hear.</p>

<h2 class="full-slugline">EXT. LEDGE - NIGHT</h2>

<p class="action">Charlie races along the slippery ledge, almost falling at 
several points. Rose then appears on the roof holding the 
Axe, still.</p>

<dl>
<dt class="character">ROSE</dt>
<dd class="parenthetical">(mostly to herself; 
<p class="action">        slurring most words)</dd></p>
<dd class="dialogue">Charlie, did you like your note? I 
thought it was pretty accurate. I 
did all the husbands' notes. I can 
forge anyone's handwriting, I can 
write in anyone's style. See, I'm an 
artist. Harriet isn't an artist. 
Sure she could get a husband, but 
she could never have done this. And 
you know what I'm most proud of?</dd>
</dl>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">What's that, Rose?</dd>
</dl>

<dl>
<dt class="character">ROSE</dt>
<dd class="dialogue">Harriet never knew. She thought they 
all just left her. I protected her. 
She's my sister.</dd>
</dl>

<p class="action">Charlie turns and runs. Rose chases him.</p>

<h2 class="full-slugline">INT. BEDROOM</h2>

<p class="action">Tony breaks into the room with his gun drawn.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(shouting)</dd>
<dd class="dialogue">Charlie!</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="parenthetical">(from closet)</dd>
<dd class="dialogue">Tony, is that you? It's me, Harriet. 
I'm in here.</dd>
</dl>

<p class="action">Carefully, Tony opens the closet door.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Tony, Rose is trying to kill Charlie. 
They're out on the ledge.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="parenthetical">(not believing)</dd>
<dd class="dialogue">Get on the floor and put your hands 
behind your back.</dd>
</dl>

<p class="action">Harriet willingly goes on the floor.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Sure, anything. You've got to save 
Charlie.</dd>
</dl>

<p class="action">Tony slaps cuffs on her and takes her to the window.</p>

<h2 class="full-slugline">INT. BEDROOM</h2>

<p class="action">Tony is standing with his back to the window, between it and 
Harriet. She looks out of the window and screams.</p>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">Look! It's Charlie!</dd>
</dl>

<p class="action">From Harriet's POV we see Charlie on the ledge edging along. 
He stops in horror when he sees Harriet, glances back to the 
pursuing Rose, and rushes off.</p>

<p class="action">Tony looks behind him out the window. Nobody is there.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Nice try.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I swear to you... It was Charlie... 
Look! Now there's Rose!</dd>
</dl>

<p class="action">Rose looks into the room, with the axe in her hand.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">No you don't.</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I beg you... Look! It is Rose.</dd>
</dl>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">Oh no, not again.</dd>
<dd class="parenthetical">(he glances at the 
<p class="action">        window)</dd></p>
<dd class="dialogue">Aaaaah! Rose.</dd>
</dl>

<h2 class="full-slugline">INT. LEVENSTEIN'S WINDOW</h2>

<p class="action">There is opera music playing. Charlie rushes by, past the 
window. There is a beat and he comes back, staring inside in 
amazement.</p>

<h5 class="goldman-slugline">REVERSE ANGLE</h5>

<p class="action">Mr. Levenstein is in a Viking outfit. Mrs. Levenstein is in 
full Norse Regalia.</p>

<h2 class="full-slugline">INT. LEVENSTEIN'S WINDOW</h2>

<p class="action">Charlie gulps and rushes on, hastily pursued by Rose.</p>

<h2 class="full-slugline">INT. THE ROOF</h2>

<p class="action">Rose pulls the Axe back and swings, and the momentum of the 
swing pulls her feet out from under her, and on the slippery 
icy roof she falls and starts to slide.</p>

<p class="action">Just as she's about to go off the fifty foot high roof, 
Charlie climbs down the roof. He stands over her. She's about 
to slip. Her hands are losing strength. Her fingers are 
slipping. The rain is falling harder and harder.</p>

<p class="action">Charlie walks over to the cage where she's hanging on for 
life.</p>

<p class="action">He leans down to help her up, but just as he grabs on to her 
hand, the drainpipe she's holding onto slips.</p>

<p class="action">She is now dangling from the roof, the rain falling harder 
and harder. Charlie now is nowhere near her. He then gets 
down on his knees on the roof and starts to climb down the 
side of the drainpipe to get her.</p>

<p class="action">Rose looks up helplessly at him. Not really asking for his 
help. Not denying it. She's accepted her fate.</p>

<p class="action">Policemen, ambulances and spectators have gathered below in 
bunches as Charlie climbs down the drainpipe, he himself 
hanging on for dear life.</p>

<p class="action">He just reaches out far enough to grab her hand, and just as 
he does, her drainpipe tears and falls into the crowd below. 
Charlie, then with all his strength &#8209;&#8209; his "where has this 
strength been my whole life" strength &#8209;&#8209; pulls her up to the 
roof next to him.</p>

<p class="action">Several policemen make their way onto the roof and come over 
to where Charlie is detaining Rose. The police take her, 
handcuff her and cart her away. From the corner of the roof 
appears Tony.</p>

<dl>
<dt class="character">TONY</dt>
<dd class="dialogue">I hate to bother you on your 
honeymoon, Charlie, but...</dd>
</dl>

<p class="action">Charlie looks beyond Tony and sees Harriet standing in the 
doorway. He goes over and puts his arm around Harriet.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Thank God. I'm sorry I doubted you, 
but I thought you were the killer, 
but you were acting pretty strange?</dd>
</dl>

<dl>
<dt class="character">HARRIET</dt>
<dd class="dialogue">I thought you were going to leave 
me, like the others. Thank God they 
were just murdered. I thought they 
were always leaving me.</dd>
</dl>

<p class="action">Below, Rose is put into a police car and taken off. The SIRENS 
disappear. So do the crowds.</p>

<h5 class="goldman-slugline">DISSOLVE INTO:</h5>

<p class="action">THE SOUND OF A CROWD IN A CLUB:</p>

<h2 class="full-slugline">INT. SPILETTI'S COFFEE HOUSE</h2>

<p class="action">Charlie is on stage looking very beatnik. He's reading his 
poetry, but we can't hear it. He nods to someone off stage. 
Harriet is in the audience, also looking very beatnik with 
their three year old son, STUART, a miniature beatnik version 
of Charlie.</p>

<dl>
<dt class="character">CHARLIE (V.O.)</dt>
<dd class="dialogue">My dad was right. You don't lose 
your muse once you're married. Nothing 
changed, except I gained a great 
son, Stuart.</dd>
</dl>

<p class="action">SOUND UP on Charlie's poetry.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">MARRIED MAN
MOST MERRY
AND IN CONCLUSION</dd>
</dl>

<dl>
<dt class="character">CROWD AND CHARLIE</dt>
<dd class="dialogue">THIS POEM SUCKS.</dd>
</dl>

<p class="action">The crowd goes crazy.</p>

<dl>
<dt class="character">CHARLIE</dt>
<dd class="dialogue">Thank you very much.</dd>
</dl>

<p class="action">HOUSE MUSIC kicks on. It's Saturday Night by the Bay City 
Rollers.</p>

<dl>
<dt class="character">BAY CITY ROLLERS</dt>
<dd class="dialogue">S-A-T-U-R-D-A-Y NIGHT</dd>
</dl>

<p class="action">Charlie comes off stage and joins his wife and child at their 
table. He is very happy.</p>

<h3 class="right-transition">FADE OUT:</h3>

<p class="action">                THE END</p>

</div><!-- end screenplay -->
